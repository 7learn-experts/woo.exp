<?php
/*
Plugin Name: خروجی کاربران
Plugin URI: https://www.7learn.com
Description:  مدیریت خروجی کاربران
Author: Kaivan Alimohammadi<keivan.amohamadi@gmail.com>
Version: 1.0.0
Author URI: https://www.7learn.com
*/
function uexp_handle_exporter()
{

    include "adminPage.php";
}

function uexp_admin_page()
{
    add_users_page(
        'خروجی کاربران',
        'خروجی کاربران',
        'manage_options',
        'users-exporter',
        'uexp_handle_exporter'
    );

}

add_action('admin_menu', 'uexp_admin_page');
function uexp_exporter(){
    $format = $_POST['format'];
    $exporterClass = $format.'Exporter';
    include 'classes/'.$exporterClass.'.php';
    $exporterInstance = new $exporterClass;
    $exporterInstance->export();
    exit;
}
add_action('admin_post_exportData','uexp_exporter');