<?php

include "Exporter.php";

class CsvExporter extends Exporter
{

    public function export()
    {
        $data = $this->getData();
        include "php-export-data.class.php";
        $exporter = new ExportDataExcel('browser', 'users.xls');
        $exporter->initialize();
        $exporter->addRow(['ID','login','display_name']);
        foreach ($data as $user) {
            $row = [
                $user->ID,
                $user->user_login,
                $user->display_name,
            ];
            $exporter->addRow($row);
        }
        $exporter->finalize();
    }
}