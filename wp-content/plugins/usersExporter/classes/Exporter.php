<?php


abstract class Exporter
{
    private $data;

    public function __construct()
    {
        $this->prepareData();
    }

    public function prepareData()
    {
        global $wpdb;
        $this->data = $wpdb->get_results("SELECT * FROM {$wpdb->users}");
    }
    public function getData()
    {
        return $this->data;
    }
    abstract public function export();
}