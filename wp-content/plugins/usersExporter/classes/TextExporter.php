<?php
include "Exporter.php";

class TextExporter extends Exporter
{

    public function export()
    {
      $data = $this->getData();
        $exportedData = '';
        foreach ($data as $user)
        {
            $exportedData.=$user->display_name.PHP_EOL;
        }
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename('users.txt'));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . strlen($exportedData));
        echo $exportedData;
    }
}