<div class="wrap">
    <h1>خروجی کاربران</h1>
    <form action="<?php echo admin_url('admin-post.php') ?>" method="post">
        <input type="hidden" name="action" value="exportData">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">نوع داده ها:</th>
                <td>
                    <label for="textExport">متنی : </label>
                    <input type="radio" name="format" id="textExport" value="Text" checked>
                    <label for="csvExport">اکسل : </label>
                    <input type="radio" name="format" id="csvExport" value="Csv">
                </td>
            </tr>
        </table>
        <?php submit_button('دریافت خروجی','submit','exportData'); ?>
    </form>
</div>