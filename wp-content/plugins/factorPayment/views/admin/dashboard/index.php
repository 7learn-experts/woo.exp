<div class="wrap">
	<h1>لیست فاکتور ها</h1>
    <style>
        .wpf_status{
            color: #ffffff;
            padding: 2px 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-size: 9px;

        }
        .wpf_status_success{
            background-color: #0f990a;
        }
        .wpf_status_error{
            background-color: #bc1011;
        }
    </style>
	<table class="widefat">
		<?php wpf_load_view('admin.dashboard.columns') ?>
		<?php if($factors && count($factors) > 0): ?>
			<?php foreach ($factors as $factor): ?>
				<?php wpf_load_view('admin.dashboard.item',compact('factor')) ?>
			<?php endforeach; ?>
		<?php else: ?>
			<?php wpf_load_view('admin.dashboard.no_item') ?>
		<?php endif; ?>
	</table>
</div>