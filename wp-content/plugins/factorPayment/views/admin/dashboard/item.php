<tr>
	<td><?php echo $factor->display_name; ?></td>
	<td><?php echo wpf_format_amount($factor->factor_amount); ?></td>
	<td><?php echo wpf_persian_date($factor->factor_created_at); ?></td>
	<td><?php echo $factor->factor_updated_at; ?></td>
	<td><?php echo $factor->factor_expired_at; ?></td>
	<td><?php echo wpf_status($factor->factor_status); ?></td>
	<td>
        <a target="_blank" title="نمایش فاکتور" href="<?php echo home_url('/factor/'.$factor->factor_code); ?>"><span class="dashicons dashicons-external"></span></a>
    </td>
</tr>