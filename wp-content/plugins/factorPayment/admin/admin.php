<?php
add_action( 'admin_menu', 'wpf_add_menu_page' );

function wpf_add_menu_page() {
	add_menu_page(
		'فاکتور ها',
		'فاکتور ها',
		'manage_options',
		'wpf_factors',
		'wpf_main_page'
	);
	add_submenu_page(
		'wpf_factors',
		'ایجاد فاکتور ',
		'ایجاد فاکتور ',
		'manage_options',
		'wpf_new_factor',
		'wpf_new_factor_page'
	);
}

function wpf_main_page() {
	global $wpdb, $table_prefix;
	$factors = $wpdb->get_results( "
			SELECT 
			f.*,
			u.display_name 
			FROM {$table_prefix}factors f
			JOIN {$wpdb->users} u ON f.factor_user_id=u.ID
" );
	wpf_load_view( 'admin.dashboard.index', compact( 'factors' ) );
}

function wpf_new_factor_page() {
	global $wpdb, $table_prefix;
	if ( isset( $_POST['save_factor'] ) ) {
		$user_id     = $_POST['user_id'];
		$amount      = $_POST['amount'];
		$description = $_POST['description'];
		$expired_at  = $_POST['expired_at'];

		$newFactorData = [
			'factor_code'        => wpf_generate_code(),
			'factor_user_id'     => $user_id,
			'factor_amount'      => $amount,
			'factor_description' => $description,
			'factor_created_at'  => date( 'Y-m-d H:i:s' ),
			'factor_updated_at'  => date( 'Y-m-d H:i:s' ),
			'factor_expired_at'  => ! empty( $expired_at ) ? $expired_at : null,
			'factor_status'      => 0 // 0 => unpaid , 1 => paid

		];
		$insert_result = $wpdb->insert( $table_prefix . 'factors', $newFactorData, [
			'%s',
			'%d',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
		] );
		if ( $insert_result == false ) {
			print $wpdb->last_error;
		}

	}
	$actions = apply_filters( 'wpf_actions', [
		1 => [
			'title'    => 'ثبت نام کاربر',
			'callback' => 'wpf_register_vip_user'
		],
		2 => 'wpf_purchase_product',
		3 => 'wpf_subscribe_course'
	] );
	wpf_load_view( 'admin.factor.new' );
}
