<?php
function wpf_load_view( $view, $params = array() ) {
	$view         = str_replace( '.', DIRECTORY_SEPARATOR, $view );
	$viewFilePath = WPF_VIEWS . $view . '.php';
	if ( file_exists( $viewFilePath ) && is_readable( $viewFilePath ) ) {
		! empty( $params ) ? extract( $params ) : null;
		include $viewFilePath;
	}
}

function wpf_generate_code( $length = 10 ) {
	return bin2hex( random_bytes( $length / 2 ) );
}

function wpf_status( $status ) {
	$cssClass   = 'wpf_status_error';
	$statusText = 'پرداخت نشده';
	if ( $status == 1 ) {
		$cssClass   = 'wpf_status_success';
		$statusText = 'پرداخت شده';
	}

	return "<span class='wpf_status {$cssClass}'>{$statusText}</span>";
}

function wpf_format_amount( $amount ) {

	$result = number_format( $amount );

	return wpf_persian_number( $result );
}

function wpf_persian_number( $input ) {
	$persian_numbers = [ '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' ];
	$en_numbers      = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];

	return str_replace( $en_numbers, $persian_numbers, $input );

}

function wpf_persian_date( $en_date ) {
	list( $en_date, $en_time ) = explode( ' ', $en_date );
	list( $y, $m, $d ) = explode( '-', $en_date );
	$persian_date = gregorian_to_jalali( $y, $m, $d );
	$result       = implode( '-', $persian_date ) . ' ' . $en_time;

	return wpf_persian_number( $result );
}

function wpf_get_current_url() {
	return $_SERVER['REQUEST_URI'];
}

function wpf_show_factor( $factor_code ) {
	global $wpdb, $table_prefix;
	$factor_item = $wpdb->get_row( $wpdb->prepare(
		"
		SELECT * 
		FROM {$table_prefix}factors
		WHERE factor_code=%s
	", $factor_code ) );

	if ( is_null( $factor_item ) ) {
		wp_redirect( '/' );
		exit;
	}
	if ( isset( $_POST['doPayment'] ) ) {
		$payment_insert_result = $wpdb->insert( $table_prefix . 'factor_payments', [
			'payment_factor_id'  => $factor_item->factor_id,
			'payment_amount'     => $factor_item->factor_amount,
			'payment_gateway'    => 'سامان',
			'payment_res_num'    => wpf_generate_res_num(),
			'payment_created_at' => date( 'Y-m-d H:i:s' ),
			'payment_status'     => 0
		] );
		if ( $payment_insert_result ) {
			$payment_id = $wpdb->insert_id;
			wpf_saman_payment_request( $payment_id );
		}

	}
	wpf_load_view( 'front.factor.detail', compact( 'factor_item' ) );
}

function wpf_verify_factor() {
	global $wpdb, $table_prefix;
	$state         = $_POST['State'];
	$ref_num       = $_POST['RefNum'];
	$res_num       = $_POST['ResNum'];
	$trace_number  = $_POST['TRACENO'];
	$payment_item  = $wpdb->get_row( $wpdb->prepare( "
			SELECT *
			FROM {$table_prefix}factor_payments
			WHERE payment_res_num=%s
	", $res_num ) );
	$verify_result = wpf_saman_payment_verify( $state, $ref_num, $res_num, $payment_item->payment_amount );
	if ( $verify_result ) {
		do_action( 'wpf_factor_paid', $payment_item->payment_factor_id );
		$wpdb->update( $table_prefix . 'factor_payments', [
			'payment_ref_num' => $trace_number,
			'payment_paid_at' => date( 'Y-m-d H:i:s' ),
			'payment_status'  => 1,
		], [
			'payment_id' => $payment_item->payment_id
		], [ '%s', '%s', '%d' ], [ '%d' ] );
		$wpdb->update( $table_prefix . 'factors', [
			'factor_status' => 1
		], [
			'factor_id' => $payment_item->payment_factor_id
		], [ '%d' ], [ '%d' ] );
	}
	wpf_load_view( 'front.payment.result', compact( 'verify_result', 'payment_item', 'trace_number' ) );
}

function wpf_generate_res_num() {
	return (int) microtime( true );
}

function wpf_send_notifications( $factor_id ) {
	global $wpdb, $table_prefix;
	$factor = $wpdb->get_row( $wpdb->prepare( "
		SELECT * 
		FROM {$table_prefix}factors
		WHERE factor_id=%d	
	", $factor_id ) );
	if ( ! is_null( $factor ) ) {
		$user_mobile = get_user_meta( $factor->factor_user_id, 'mobile', true );//'09123456789';
		$sms_message = 'فاکتور با شماره #factor_id# با موفقیت پرداخت شد.';
		$sms_message = str_replace( '#factor_id#', $factor_id, $sms_message );
		$user_email  = ( get_user_by( 'ID', $factor->factor_user_id ) )->user_email;
		wpf_send_sms( $user_mobile, $sms_message );
		wpf_send_email( $user_email, $sms_message );
	}
}

function wpf_send_sms( $to, $message ) {
	$config  = wpf_get_config();
	$un      = $config['sms']['user_name'];
	$pss     = $config['sms']['password'];
	$from    = $config['number'];
	$message = urlencode( $message );
	if ( ! is_array( $to ) ) {
		$numbers = array( $to );
	}
	$numbers = array_unique( $numbers );
	foreach ( $numbers as $number ) {
		$to  = $number;
		$url = "http://www.sibsms.com/APISend.aspx?" . "Username=" . $un . "&Password=" . $pss . "&From=" . $from . "&To=" . $to . "&Text=" . $message;
		file_get_contents( $url );

	}


}

function wpf_send_email( $user_email, $message ) {
	$headers[] = 'From: Website Support <support@website.com>';
	wp_mail( $user_email, 'پرداخت فاکتور', $message, $headers );
}

function wpf_get_config() {
	$config_file_path = WPF_INC . 'config.php';
	$config_data      = include $config_file_path;

	return $config_data;

}