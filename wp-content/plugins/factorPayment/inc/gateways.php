<?php
function wpf_saman_payment_request( $payment_id ) {
	global $wpdb, $table_prefix;
	$payment_item = $wpdb->get_row( $wpdb->prepare( "
		SELECT *
		FROM {$table_prefix}factor_payments
		WHERE payment_id=%d
	", $payment_id ) );
	if ( is_null( $payment_item ) ) {
		return false;
	}
	$config       = wpf_get_config();
	$mid          = $config['gateways']['saman']['mid'];
	$amount       = $payment_item->payment_amount;
	$res_num      = $payment_item->payment_res_num;
	$callback_url = home_url( '/payment/saman/verify' );
	$send_atu     = "<script language='JavaScript' type='text/javascript'>document.getElementById('checkout_confirmation').submit();</script>";
	echo '<form id="checkout_confirmation"  method="post" action="https://sep.shaparak.ir/Payment.aspx" style="margin:0px"  >
		<input type="hidden" id="Amount" name="Amount" value="' . esc_attr( $amount ) . '">
		<input type="hidden" id="MID" name="MID" value="' . esc_attr( $mid ) . '">
		<input type="hidden" id="ResNum" name="ResNum" value="' . esc_attr( $res_num ) . '">
		<input type="hidden" id="RedirectURL" name="RedirectURL" value="' . esc_attr( $callback_url ) . '">
		</form>' . $send_atu;
}

function wpf_saman_payment_verify( $state, $ref_num, $res_num, $factorAmount ) {

	if ( $state != 'OK' ) {
		return false;
	}
	$config      = wpf_get_config();
	$mid         = $config['gateways']['saman']['mid'];
	$soap_client = new SoapClient( 'https://sep.shaparak.ir/payments/referencepayment.asmx?wsdl' );
	$amount      = $soap_client->VerifyTransaction( $ref_num, $mid );
	if ( $amount <= 0 ) {
		return false;
	}
	if ( $factorAmount != $amount ) {
		return false;
	}

	return true;
}