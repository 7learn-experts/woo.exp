<?php
return [
	'gateways'  => [
		'saman' => [
			'mid'      => 'xxxxxxxx',
			'password' => 'xxxxxxxx'
		]
	],
	'sms'       => [
		'user_name' => 'xxxxxxxx',
		'password'  => 'xxxxxxxx',
		'number'    => 'xxxxxxxx'
	],
	'google'    => [
		'maps'      => [
			'app_key'    => '',
			'app_secret' => ''
		],
		'recaptcha' => [
			'app_key'    => '',
			'app_secret' => ''
		]
	],
	'instagram' => [
		''
	]
];