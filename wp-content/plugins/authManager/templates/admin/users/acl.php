<div class="wrap">
	<h1>دسترسی ها</h1>
	<style>
		.user-cap-label
		{
			display: inline-block;
			margin: 10px;
		}
	</style>
	<form action="" method="get" id="userForm">
		<input type="hidden" name="page" value="auth_users_acl">
		<table class="form-table">
			<tr valign="top">
					<th scope="row">کاربر :</th>
					<td>
						<select name="user" id="user">
							<?php foreach ($users as $user): ?>
								<option
									value="<?php echo $user->ID; ?>"
									<?php selected($selected_user?$selected_user->ID:0,$user->ID); ?>
								><?php echo $user->display_name; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">دسترسی ها</th>
				<td>
					<div class="user-caps">
						<?php if(!is_null($selected_user)): ?>
							<?php foreach ($selected_user->allcaps as $cap => $value): ?>
								<label class="user-cap-label" for="<?php echo $cap; ?>">
									<?php echo $cap; ?>
									<input class="user-cap-input" type="checkbox" id="<?php echo $cap; ?>" name="caps[]" value="<?php echo $cap; ?>">
								</label>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</td>
			</tr>
		</table>

	</form>
	<script>
		jQuery(document).ready(function($){
            var frm = $('#userForm');
		    $('#user').on('change',function(event){
		        frm.submit();
		        // var _this = $(this);
		        // var user_id = _this.val();
				// frm.append('<input type="hidden" id="" name="user_id" value="'+user_id+'">');
		    });
		});
	</script>
</div>