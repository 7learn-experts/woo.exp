<div class="wrap">
	<h1><?php _e('Users Manager','wp_athm'); ?></h1>
	<table class="widefat">
		<tr>
			<th>نام</th>
			<th>ایمیل</th>
			<th>شماره تماس</th>
			<th>موجودی</th>
			<th>عملیات</th>
		</tr>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $user->display_name; ?></td>
				<td><?php echo $user->user_email ?></td>
				<td><?php echo get_user_meta($user->ID,'mobile',true); ?></td>
				<td></td>
				<td>
                    <a title="ویرایش اطلاعات کاربر" href="<?php echo  add_query_arg(['action' => 'edit_user','user_id' => $user->ID]); ?>"><span class="dashicons dashicons-edit"></span></a>
                </td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>