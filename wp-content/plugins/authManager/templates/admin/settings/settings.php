<div class="wrap">
	<h1><?php _e('Settings','wp_athm'); ?></h1>
	<form action="" method="post">
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><?php _e('Login & Register Pages Template:','wp_athm'); ?></th>
				<td>
					<?php foreach ($templates as $template => $title): ?>
						<input
							type="radio"
							name="athm_template"
							value="<?php echo $template; ?>"
							<?php checked($default_template,$template) ;?>
						>
						<span><?php echo $title; ?></span>
					<?php endforeach; ?>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Login page address: ','wp_athm'); ?></th>
				<td>
                    <input
                            type="text"
                            name="athm_login_path"
                            value="<?php echo $login_path; ?>"
                    ><code>auth/</code>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Register page address: ','wp_athm'); ?></th>
				<td>
                    <input
                            type="text"
                            name="athm_register_path"
                            value="<?php echo $register_path; ?>"
                    ><code>auth/</code>
				</td>
			</tr>
		</table>
		<?php submit_button('ذخیره اطلاعات','primary','athm_save_options'); ?>
	</form>
</div>