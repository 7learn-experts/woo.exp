<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>صفحه ثبت نام کاربران</title>
	<link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/normalize.css' ?>">
	<link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/milligram-rtl.css' ?>">
	<link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/custom.css' ?>">
    <link rel="stylesheet" href="<?php $template = get_option('athm_default_template'); echo ATHM_ASSETS."presets/$template.css"; ?>">

</head>
<body>
<div class="container">
	<div class="row">
		<div class="column column-50 column-offset-25">
			<?php if ( $hasError ): ?>
                <ul>
					<?php foreach ( $errorMessages as $error ): ?>
                        <li class="redText"><?php echo $error; ?></li>
					<?php endforeach; ?>
                </ul>
			<?php endif; ?>
			<form action="" method="post">
				<fieldset>
					<label for="userEmail">ایمیل :</label>
					<input type="text" name="userEmail" id="userEmail">
					<label for="userPassword">کلمه عبور :</label>
					<input type="password" name="userPassword" id="userPassword">
                    <?php wp_nonce_field('athm_user_login','athm_user_login_nonce'); ?>
					<input class="button-primary" name="doLogin" type="submit" value="ورود">
				</fieldset>
			</form>
            <a href="<?php echo wp_nonce_url(esc_url(add_query_arg(['action' => 'reset'],home_url()))); ?>">فراموشی رمز عبور</a>
		</div>
	</div>
</div>
</body>
</html>