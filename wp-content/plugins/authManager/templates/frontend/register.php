<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>صفحه ثبت نام کاربران</title>
    <link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/normalize.css' ?>">
    <link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/milligram-rtl.css' ?>">
    <link rel="stylesheet" href="<?php echo ATHM_ASSETS . 'css/custom.css' ?>">
    <link rel="stylesheet" href="<?php $template = get_option('athm_default_template'); echo ATHM_ASSETS."presets/$template.css"; ?>">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="column column-50 column-offset-25">
			<?php if ( $hasError ): ?>
                <ul>
					<?php foreach ( $errorMessages as $error ): ?>
                        <li class="redText"><?php echo $error; ?></li>
					<?php endforeach; ?>
                </ul>
			<?php endif; ?>
            <?php if($isSuccess): ?>
            <div class="">
                <p>ثبت نام شما با موفقیت انجام شد.</p>
            </div>
            <?php endif; ?>
            <form action="" method="post">
                <fieldset>
                    <label for="userFullName">نام و نام خانوادگی :</label>
                    <input type="text" name="userFullName" id="userFullName" >
                    <label for="userEmail">ایمیل :</label>
                    <input type="text" name="userEmail" id="userEmail">
                    <label for="userPassword">کلمه عبور :</label>
                    <input type="password" name="userPassword" id="userPassword">
                    <input class="button-primary" name="saveRegisterForm" type="submit" value="ثبت نام">
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>