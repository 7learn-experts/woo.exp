<?php
function athm_check_login( $email, $password ) {
	$is_email = is_email( $email );
	if ( ! $is_email ) {
		return false;
	}
	$user = wp_authenticate_email_password( null, $email, $password );
	if ( is_wp_error( $user ) ) {
		return false;
	}

	return $user;
}
function athm_update_logout_url( $logout_url, $redirect ) {
	return home_url( '/auth/logout' );
}
add_filter( 'logout_url', 'athm_update_logout_url', 10, 2 );
