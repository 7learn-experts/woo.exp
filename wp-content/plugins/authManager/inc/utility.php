<?php
function athm_load_admin_template($template,$params=array())// ['users' => 1,'admins' => 2]
{
	$template = str_replace('.','/',$template);
	extract($params);// $users=1,$admins=2
	include ATHM_TPL_ADMIN.$template.'.php';

}

function athm_load_frontend_template($template,$params=array())
{
	$template = str_replace('.','/',$template);
	extract($params);
	include ATHM_TPL_FRONT.$template.'.php';
}

function athm_load_tpl($template,$params=array(),$type='admin')
{
	$template = str_replace('.','/',$template);
	extract($params);
	$base_path = $type == 'admin' ? ATHM_TPL_ADMIN : ATHM_TPL_FRONT;
	include $base_path.$template.'.php';
}

function athm_is_admin()
{
	return current_user_can('manage_options');
}

function athm_redirect($url)
{
	wp_redirect($url);
	exit;
}