<?php
/*
Plugin Name: AuthManager
Plugin URI: https://www.7learn.com
Description:  مدیریت ثبت نام و ورود کاربران
Author: Kaivan Alimohammadi<keivan.amohamadi@gmail.com>
Version: 1.0.0
Author URI: https://www.7learn.com
Text Domain: wp_athm
Domain Path: /languages/
*/

defined( 'ABSPATH' ) || die( 'access denied!' );

define( 'ATHM_DIR', plugin_dir_path( __FILE__ ) );
define( 'ATHM_URL', plugin_dir_url( __FILE__ ) );
define( 'ATHM_TPL_FRONT', ATHM_DIR . 'templates/frontend/' );
define( 'ATHM_TPL_ADMIN', ATHM_DIR . 'templates/admin/' );
define( 'ATHM_INC', ATHM_DIR . 'inc/' );
define( 'ATHM_ADMIN', ATHM_DIR . 'admin/' );
define( 'ATHM_ASSETS', ATHM_URL . 'assets/' );
include ATHM_INC . 'utility.php';
include ATHM_INC . 'functions.php';
include ATHM_ADMIN . 'admin.php';

add_action('plugins_loaded','athm_load_translation');

function athm_load_translation()
{
    load_plugin_textdomain( 'wp_athm', false, basename( dirname( __FILE__ ) ) . '/languages' );
}



function authManagerActivation() {
	$default_template = get_option( 'athm_default_template', false );
	if ( ! $default_template ) {
		update_option( 'athm_default_template', 'red' );
	}
	update_option( 'athm_login_path', 'login' );
	update_option( 'athm_register_path', 'register' );
	add_role(
		'shop_manager',
		'Shop Manager',
		array(
			'read'           => true,
			'view_orders'    => true,
			'delete_orders'  => true,
			'confirm_orders' => true
		)
	);

}

function authManagerDeactivation() {

}

register_activation_hook( __FILE__, 'authManagerActivation' );
register_deactivation_hook( __FILE__, 'authManagerDeactivation' );
//register_uninstall_hook();
function checkAuth( $url = '/' ) {
	if ( is_user_logged_in() ) {
		wp_redirect( $url );
		exit;
	}
}

function authManagerCheckUrls() {
	$currentUrl    = $_SERVER['REQUEST_URI'];
	$auth_prefix   = 'auth/';
	$login_path    = $auth_prefix . get_option( 'athm_login_path', 'login' );
	$register_path = $auth_prefix . get_option( 'athm_register_path', 'register' );

//	$urls = ['login','register'];
	$hasError      = false;
	$isSuccess     = false;
	$errorMessages = [];
	if ( strpos( $currentUrl, $register_path ) !== false ) {
		checkAuth();

		if ( isset( $_POST['saveRegisterForm'] ) ) {
			$userFullName = $_POST['userFullName'];
			$userEmail    = $_POST['userEmail'];
			$userPassword = $_POST['userPassword'];
			if ( empty( $userFullName ) ) {
				$hasError        = true;
				$errorMessages[] = "پر کردن فیلد نام الزامی می باشد.";
//				array_push($errorMessages,""); //alternative
			}
			$emailValidationResult = filter_var( $userEmail, FILTER_VALIDATE_EMAIL );
			$isValidaEmail         = ! empty( $emailValidationResult );
			if ( ! $isValidaEmail || email_exists( $userEmail ) ) {
				$hasError        = true;
				$errorMessages[] = "این ایمیل در دسترس نمی باشد.";
			}
			if ( strlen( $userPassword ) < 6 ) {
				$hasError        = true;
				$errorMessages[] = "کلمه عبور معتبر نمی باشد.";
			}
			if ( ! $hasError ) {
				list( $preAt, $postAt ) = explode( '@', $userEmail );
				$userLogin          = $preAt . rand( 1000, 9999 );
				$userData           = [
					'user_login'   => apply_filters( 'pre_user_login', $userLogin ),
					'display_name' => apply_filters( 'pre_user_display_name', $userFullName ),
					'user_email'   => apply_filters( 'pre_user_email', $userEmail ),
					'user_pass'    => apply_filters( 'pre_user_pass', $userPassword )
				];
				$userRegisterResult = wp_insert_user( $userData );
				if ( is_wp_error( $userRegisterResult ) ) {
					$hasError        = true;
					$errorMessages[] = "خطایی در ثبت نام شما رخ داده است.";
				} else {
					$isSuccess = true;
					do_action( 'user_register', $userRegisterResult );
					wp_redirect( home_url( 'auth/login' ) );
					exit;
				}
			}

		}
		include ATHM_TPL_FRONT . 'register.php';
		exit;
	}
	if ( strpos( $currentUrl, $login_path ) !== false ) {
		checkAuth();
		if ( isset( $_POST['doLogin'] ) ) {

			if (
				! isset( $_POST['athm_user_login_nonce'] )
				|| ! wp_verify_nonce( $_POST['athm_user_login_nonce'], 'athm_user_login' )

			) {
				wp_die( 'درخواست شما معتبر نمی باشد' );
			}

			$userEmail    = strip_tags( $_POST['userEmail'] );// sanitize
			$userPassword = $_POST['userPassword'];
			$user         = athm_check_login( $userEmail, $userPassword );
			if ( $user == false ) {
				$hasError        = true;
				$errorMessages[] = 'نام کاربری یا کلمه عبور اشتباه می باشد.';
			}

			if ( ! $hasError ) {
				$userLoginData = [
					'user_login'    => $user->user_login,
					'user_password' => $userPassword
				];
				$login_result  = wp_signon( $userLoginData );
				if ( is_wp_error( $login_result ) ) {
					$hasError        = true;
					$errorMessages[] = "خطایی در عملیات لاگین اتفاق افتاده است.بعدا امتحان کنید";
				} else {
					wp_redirect( home_url( '/wp-admin' ) );
				}
			}


		}
		include ATHM_TPL_FRONT . "login.php";
		exit;
	}
	if ( strpos( $currentUrl, 'auth/logout' ) !== false ) {
		wp_logout();
		wp_redirect( home_url( '/auth/login' ) );
		exit;
	}
	if ( strpos( $currentUrl, 'athm/dashboard' ) !== false ) {
		if ( ! athm_is_admin() ) {
			athm_redirect( '/' );
		}
		if ( isset( $_POST['athm_save_options'] ) ) {
			$default_template = isset( $_POST['athm_template'] ) ? $_POST['athm_template'] : 'red';
			update_option( 'athm_default_template', $default_template );
		}
		$default_template = get_option( 'athm_default_template' );
		$templates        = [
			'red'    => 'قالب قرمز',
			'blue'   => 'قالب آبی',
			'purple' => 'قالب بنفش'
		];
		athm_load_tpl( 'dashboard.index', compact( 'templates', 'default_template' ), 'frontend' );
		exit;
	}
}

function disableWpLoginPage() {
	$currentUrl = $_SERVER['REQUEST_URI'];
	if ( strpos( $currentUrl, 'wp-login.php' ) !== false ) {
		wp_redirect( home_url( '/auth/register' ) );
		exit;
//		die('access denied.');
	}
}

add_action( 'parse_request', 'authManagerCheckUrls' );
add_action( 'init', 'disableWpLoginPage' );


