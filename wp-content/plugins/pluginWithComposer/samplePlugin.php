<?php
/*
Plugin Name: ComposerPlugin
Plugin URI: https://www.7learn.com
Description: کدنویسی ئلاگین با استفاده از composer
Author: Kaivan Alimohammadi<keivan.amohamadi@gmail.com>
Version: 1.0.0
Author URI: https://www.7learn.com
*/

include_once "vendor/autoload.php";

$crawler = new \Application\Frontend\DataGrabber();

//$crawler->extractCourseTitles();

