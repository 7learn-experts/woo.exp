<?php

namespace Application\Frontend;

use Goutte\Client;

class DataGrabber
{
    private $client;
    private $crawler;

    public function __construct()
    {
        $this->client  = new Client();
        $this->client->setClient(new \GuzzleHttp\Client(array(
            'timeout' => 90,
            'verify'  => false,
        )));
        $this->crawler = $this->client->request('GET', 'https://www.7learn.com//course');
    }

    public function extractCourseTitles()
    {
        $this->crawler->filter('.pBoxTitle > a')->each(function ($node) {
            print $node->text() . "<br>";
        });
    }
}