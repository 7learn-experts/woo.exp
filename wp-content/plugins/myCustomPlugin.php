<?php
/*
Plugin Name: Custom Admin Page
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Matt Mullenweg
Version: 1.6
Author URI: http://ma.tt/
*/

add_action('admin_menu','create_admin_menu');

function create_admin_menu(){
	add_menu_page('لیست سفارش ها','لیست سفارش ها','manage_options','orders','show_orders_content');
}
function show_orders_content(){

}