<?php

namespace System\Router;

use Application\PagesHandlers\DashboardHandler;
use Application\PagesHandlers\ProfileHandler;

class Router
{
    private $routes;

    public function __construct()
    {
        $this->setDefaultRoutes();
    }

    private function setDefaultRoutes()
    {
        $this->routes = apply_filters('ap_routes', [
            '/panel/dashboard' => DashboardHandler::class,
            '/panel/profile'   => ProfileHandler::class
        ]);
    }

    public function boot()
    {
        $currentRoute = $this->getCurrentRoute();
        if (in_array($currentRoute, $this->getRouteKeys())) {
            $handler         = $this->getRouteHandler($currentRoute);
            $handlerInstance = new $handler;
            $handlerInstance->index();
            exit;
        }
    }

    private function getCurrentRoute()
    {
        return strtok($_SERVER['REQUEST_URI'], '?');
    }

    private function getRouteKeys()
    {
        return array_keys($this->routes);
    }


    private function getRouteHandler($route)
    {
        return $this->routes[$route];
    }

    public function getRoutes()
    {
        return $this->routes;
    }
}