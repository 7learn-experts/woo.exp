<?php


namespace System\Http;


class Request
{
    private $data;

    public function __construct()
    {
        $this->data = $_REQUEST;
    }

    public function sanitize($input)
    {
        return sanitize_text_field($this->data[$input]);
    }

    public function all()
    {
        return $this->data;
    }

    public function sanitize_all()
    {

    }

    public function exists($input)
    {
        return isset($this->data[$input]) && ! empty($this->data[$input]);
    }
}