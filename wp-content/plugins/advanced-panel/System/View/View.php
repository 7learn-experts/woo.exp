<?php


namespace System\View;


class View
{
    public static function render($view,$params=array(),$layout = null)
    {
       $viewFile = !empty($layout) ? $layout : $view;
       $viewPath = str_replace('.',DIRECTORY_SEPARATOR,$viewFile);
       $viewPath.='.php';
       $fullViewPath = AP_VIEW.$viewPath;
       if(file_exists($fullViewPath) && is_readable($fullViewPath))
       {

           !empty($layout) ? $params['view'] = self::buildViewPath($view) : null;
           !empty($params) ? extract($params) : null;
           include $fullViewPath;
       }
    }

    public static function load($view,$params = array())
    {
        self::render($view,$params,'layouts.main');
    }

    public static function buildViewPath($view)
    {
        return AP_VIEW .str_replace('.',DIRECTORY_SEPARATOR,$view).'.php';
    }
}