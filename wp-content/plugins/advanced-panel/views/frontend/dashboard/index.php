<div class="card-title">
    <h4>آمار و اطلاعات </h4>
</div>
<div class="card-body">
    <div class="row">
        <div class="col-md-3">
            <div class="card p-30">
                <div class="media">
                    <div class="media-right meida media-middle">
                        <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body media-text-left">
                        <h2><?php echo \Application\Utility\PersianConverter::currency($currentMetrics->balance()); ?></h2>
                        <p class="m-b-0">کیف پول</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card p-30">
                <div class="media">
                    <div class="media-right meida media-middle">
                        <span><i class="fa fa-comments f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body media-text-left">
                        <h2><?php echo \Application\Utility\PersianConverter::number($currentMetrics->commentsCount()); ?></h2>
                        <p class="m-b-0">دیدگاه ها</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card p-30">
                <div class="media">
                    <div class="media-right meida media-middle">
                        <span><i class="fa fa-book f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body media-text-left">
                        <h2><?php echo \Application\Utility\PersianConverter::number($currentMetrics->postsCount()); ?></h2>
                        <p class="m-b-0">مطالب</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card p-30">
                <div class="media">
                    <div class="media-right meida media-middle">
                        <span><i class="fa fa-star f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body media-text-left">
                        <h2><?php echo \Application\Utility\PersianConverter::number($currentMetrics->totalPoints()); ?></h2>
                        <p class="m-b-0">امتیاز</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>