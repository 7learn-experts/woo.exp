<div class="card-title">
    پروفایل
</div>
<div class="card-body">
    <div class="row">
        <a class="btn btn-success btn-rounded m-b-10 m-l-5 "
           href="<?php echo ap_add_args(['action' => 'increaseBalance']); ?>">افزایش موجودی</a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="basic-form p-10">
                <form action="" method="post">
                    <input type="hidden" name="action" value="saveProfile">
                    <div class="form-group">
                        <label for="userFullName">نام و نام خانوادگی</label>
                        <input id="userFullName" name="userFullName" type="text"
                               class="form-control input-default hasPersianPlaceHolder"
                               value="<?php echo $currentUser->display_name ?>"
                        >
                    </div>
                    <div class="form-group">
                        <label for="userEmail">آدرس ایمیل</label>
                        <input id="userEmail" name="userEmail" type="text"
                               class="form-control input-default hasPersianPlaceHolder"
                               value="<?php echo $currentUser->user_email ?>"
                               readonly
                        >
                    </div>
                    <div class="form-group">
                        <label for="userPassword">کلمه عبور</label>
                        <input id="userPassword" name="userPassword" type="password"
                               class="form-control input-default hasPersianPlaceHolder"
                        >
                    </div>
                    <div class="form-group">
                        <label for="userAddress">آدرس</label>
                        <input id="userAddress" name="userAddress" type="text"
                               class="form-control input-default hasPersianPlaceHolder"
                               value="<?php echo esc_attr(get_user_meta($currentUser->ID, '_address', true)); ?>"
                        >
                    </div>
                    <?php wp_nonce_field('ap_user_profile_update', 'ap_user_profile_nonce'); ?>
                    <div class="form-group m-t-20">
                        <button type="submit" name="submitForm" class="btn btn-primary m-b-10 m-l-5">ثبت اطلاعات
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>