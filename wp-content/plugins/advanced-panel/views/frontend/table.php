<div class="card-title">
    <h4>آمار و اطلاعات </h4>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>نام و نام خانوادگی</th>
                <th>ایمیل</th>
                <th>تاریخ ثبت نام</th>
                <th>وضعیت</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>مهرداد سامی</td>
                <td>msaami@gmail.com</td>
                <td>۱۳۹۷/۰۳/۲۲</td>
                <td><span class="badge badge-primary">فعال</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>