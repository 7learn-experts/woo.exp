<?php


class Autoloader
{

    public static function autoload($className)
    {
        $classPath     = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $classPath     .= '.php';
        $classFullPath = AP_DIR . $classPath;
        if (file_exists($classFullPath) && is_readable($classFullPath)) {
            include_once $classFullPath;
        }
    }

}