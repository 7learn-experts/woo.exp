<?php


namespace Application\Services;


class UserMetricsService
{
    private $userID;

    public function __construct($userID)
    {
        $this->userID = $userID;
    }

    public function commentsCount()
    {
        global $wpdb;
        $comment_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(comment_ID) AS total FROM $wpdb->comments WHERE comment_approved = 1 AND user_id = %s",
            $this->userID));

        return $comment_count;
    }

    public function postsCount()
    {
        return (int)count_user_posts($this->userID);
    }

    public function totalPoints()
    {
        return (int)get_user_meta($this->userID,'_total_points',true);
    }

    public function balance()
    {
        return (int)get_user_meta($this->userID,'_balance',true);
    }

}