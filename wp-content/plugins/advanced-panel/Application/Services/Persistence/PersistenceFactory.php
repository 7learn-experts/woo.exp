<?php


namespace Application\Services\Persistence;


class PersistenceFactory
{
    private $objects = [];

    public function build($name) // database,file,cloud
    {
        if (isset($this->objects[$name])) {
            return $this->objects[$name];
        }
        $persistenceClassName = ucfirst($name); // file => File
        $persistenceClassName .= 'Persistence';
        $persistenceClassName = 'Application\\Services\\Persistence\\' . $persistenceClassName;
        $object               = new $persistenceClassName;
        $this->objects[$name] = $object;

        return $this->objects[$name];
    }
}