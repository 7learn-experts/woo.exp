<?php


namespace Application\Services\Persistence;


class DatabasePersistence implements PersistenceMethod
{

    public function save(array $data)
    {
        return update_option('_ap_storage',$data);
    }
}