<?php


namespace Application\Services\Persistence;


class FilePersistence implements PersistenceMethod
{

    public function save(array $data)
    {
        $filePath = AP_FILE_STORAGE . 'storage.txt';
        if(!is_dir(AP_FILE_STORAGE))
        {
            @mkdir(AP_FILE_STORAGE);
        }
        return file_put_contents($filePath, serialize($data));
    }
}