<?php


namespace Application\Services\Persistence;


interface PersistenceMethod
{
    public function save(array $data);
}