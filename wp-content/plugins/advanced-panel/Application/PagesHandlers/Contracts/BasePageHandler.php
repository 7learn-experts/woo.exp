<?php


namespace Application\PagesHandlers\Contracts;


abstract class BasePageHandler
{
    protected $title;

    protected $icon = '';

    protected $position = 10;

    public function getTitle()
    {
        return $this->title;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getCurrentUser()
    {
        return new \WP_User(wp_get_current_user()->ID);
    }

    public function isFormSubmitted()
    {
        return isset($_REQUEST['submitForm']);
    }

    public function hasAction()
    {
        return isset($_REQUEST['action']);
    }

    public function getAction()
    {
        return $_REQUEST['action'];
    }

    public function isActionMethodExists($action)
    {
        return method_exists($this, $action);
    }

}