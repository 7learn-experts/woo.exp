<?php

namespace Application\PagesHandlers;

use Application\PagesHandlers\Contracts\BasePageHandler;
use Application\PagesHandlers\Contracts\PageHandler;
use Application\Services\UserMetricsService;
use System\View\View;

class DashboardHandler extends BasePageHandler implements PageHandler
{
    public function __construct()
    {
        $this->title = 'داشبورد';
        $this->icon  = 'tachometer';

    }

    public function index()
    {
        $currentMetrics = new UserMetricsService($this->getCurrentUser()->ID);

        View::load('frontend.dashboard.index',compact('currentMetrics'));
    }
}