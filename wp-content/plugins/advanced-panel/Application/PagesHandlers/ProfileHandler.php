<?php


namespace Application\PagesHandlers;


use Application\PagesHandlers\Contracts\BasePageHandler;
use Application\PagesHandlers\Contracts\PageHandler;
use Application\Services\UserMetricsService;
use System\View\View;

class ProfileHandler extends BasePageHandler implements PageHandler
{

    public function __construct()
    {
        $this->title = 'پروفایل';
        $this->icon  = 'user-circle';
    }

    public function index()
    {
        if ($this->hasAction()) {
            $action = $this->getAction();
            if ($this->isActionMethodExists($action)) {
                 $this->{$action}();

            }
        }
        $currentUser = $this->getCurrentUser();
        View::load('frontend.profile.index', compact('currentUser'));
    }

    public function saveProfile()
    {
        $userProfileData = array(
            'ID'           => $this->getCurrentUser()->ID,
            'display_name' => apply_filters('pre_user_display_name', sanitize_text_field($_POST['userFullName']))
        );
        if (isset($_POST['userPassword']) && ! empty($_POST['userPassword'])) {
            $userProfileData['user_pass'] = apply_filters('pre_user_pass', $_POST['userPassword']);
        }
        $userID = wp_update_user($userProfileData);
        if(isset($_POST['userAddress']) && !empty($_POST['userAddress']))
        {
            update_user_meta($userID,'_address',sanitize_text_field($_POST['userAddress']));
        }
        if ( ! is_wp_error($userID) && intval($userID) > 0) {
            return true;
        }

        return false;

    }

    public function increaseBalance()
    {

    }
}