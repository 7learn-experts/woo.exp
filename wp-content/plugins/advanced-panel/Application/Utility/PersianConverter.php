<?php


namespace Application\Utility;


class PersianConverter
{
    public static function number($input)
    {
        $persian_numbers = [ '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' ];
        $en_numbers      = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];

        return str_replace( $en_numbers, $persian_numbers, $input );
    }

    public static function currency($input)
    {
        $result = number_format($input);
        return self::number($result);
    }

    public static function toman($input)
    {
        return self::currency($input) . ' تومان ';
    }

    public static function hezarToman($input)
    {
        return self::currency($input) . ' هزار تومان ';
    }
}