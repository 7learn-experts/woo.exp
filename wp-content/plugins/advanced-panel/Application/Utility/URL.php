<?php


namespace Application\Utility;


class URL
{
    public static function addQueryArgs($args)
    {
        return esc_url(add_query_arg($args));
    }
}