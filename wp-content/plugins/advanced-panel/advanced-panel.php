<?php

/*
Plugin Name: پنل کاربری پیشرفته وردپرس
Plugin URI: https://www.7learn.com
Description:  پنل کاربری پیشرفته وردپرس
Author: Kaivan Alimohammadi<keivan.amohamadi@gmail.com>
Version: 1.0.0
Author URI: https://www.7learn.com
*/

final class AdvancedPanel
{

    private static $_instance;

    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __construct()
    {
        add_action('init', [$this, 'boot']);

    }

    /**
     * define plugin constants
     */
    private function defineConstants()
    {
        define('AP_DIR', plugin_dir_path(__FILE__));
        define('AP_URL', plugin_dir_url(__FILE__));
        define('AP_VIEW', AP_DIR . 'views/');
        define('AP_ASSETS', AP_URL . 'assets/');
        define('AP_FILE_STORAGE', AP_DIR . 'storage/');
    }

    private function ob_handler()
    {
        ob_start();
    }

    public function boot()
    {
        $this->ob_handler();
        $this->defineConstants();
        include_once "Autoloader.php";
        spl_autoload_register('Autoloader::autoload');
        $router = new \System\Router\Router();
        include_once "helpers.php";
        $router->boot();
        if (is_admin()) {
            // load admin files
            $persistence = (new \Application\Services\Persistence\PersistenceFactory())->build('database');
            $persistence->save(['id' => 25, 'price' => 6955555, 'total_items' => 6]);
        }
    }

}

AdvancedPanel::instance();