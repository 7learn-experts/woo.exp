<?php
/**
 * Created by PhpStorm.
 * User: zoc970301kavorg
 * Date: 6/11/2018
 * Time: 1:50 PM
 */

class WPCI_Invoice {

	public $id;
	public $user_id;
	public $code;
	public $expired_at;
	public $created_at;
	public $updated_at;
	public $description;
	public $paid_at_date;
	public $status;
	public $amount;
	public $title;
	public $to_do;
	/* error-fields */
	public $hasError;
	public $errors = [];
	public $status_new_invoice;
	/* error-fields */

	const NOT_PAID = 0;
	const PAID = 1;


	public function __construct() {

		$this->hasError           = false;
		$this->errors[]           = '';
		$this->status_new_invoice = null;

	}

	public function create_new_factor() {

		$this->validation();

		if ( $this->hasError ) {

			return $this;

		}
		global $wpdb, $table_prefix;
		$table_name = $table_prefix . 'invoice';

		$result = $wpdb->insert( $table_name,
			[
				'user_id'     => $this->user_id,
				'code'        => $this->code,
				'title'       => $this->title,
				'description' => $this->description,
				'amount'      => $this->amount,
				'expired_at'  => $this->expired_at,
				'created_at'  => $this->created_at,
				'updated_at'  => $this->updated_at,
				'status'      => $this->status,
				'todo'        => $this->to_do,
			]

		);
		if ( $result == 1 ) {

			$this->status_new_invoice = true;
			$this->clear_fields();

		} else {
			$this->status_new_invoice = false;
		}


		return $this;


	}

	private function validation() {


		if ( empty( $this->amount ) || ! is_numeric( $this->amount ) ) {

			$this->hasError = true;
			$this->errors[] = 'amount';
		}
		if ( empty( $this->title ) ) {

			$this->hasError = true;
			$this->errors[] = 'title';

		}

		return $this;

	}

	private function clear_fields() {


		$this->to_do       = null;
		$this->updated_at  = null;
		$this->expired_at  = null;
		$this->created_at  = null;
		$this->amount      = null;
		$this->description = null;
		$this->title       = null;

		$this->user_id  = null;
		$this->hasError = null;
		$this->errors   = null;

	}

	public function get_invoice( $page_number = null, $pages = null ) {
		global $wpdb, $table_prefix;
		$table_name = $table_prefix . 'invoice';
		$factors    = '';
		if ( $page_number != null && $pages != null ) {


			if ( $page_number <= $pages ) {
				$offset  = ( $page_number * 5 ) - 5;
				$factors = $wpdb->get_results( "SELECT f.*,u.user_email ,u.user_nicename FROM {$table_name} f 
												LEFT JOIN {$wpdb->users} u ON f.user_id=u.ID  ORDER BY f.id desc  LIMIT 5 OFFSET {$offset}" );
			} else {
				$factors = $wpdb->get_results( "SELECT f.*,u.user_email ,u.user_nicename FROM {$table_name} f 
												LEFT JOIN {$wpdb->users} u ON f.user_id=u.ID  ORDER BY f.id desc  LIMIT 5" );

			}
		} else {

			$factors = $wpdb->get_results( "SELECT f.*,u.user_email ,u.user_nicename FROM {$table_name} f 
												LEFT JOIN {$wpdb->users} u ON f.user_id=u.ID  ORDER BY f.id desc  LIMIT 5" );

		}

		return $factors;

	}

	public function get_pages() {

		global $wpdb, $table_prefix;
		$table_name    = $table_prefix . 'invoice';
		$count_factors = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name}" );
		$pages         = $count_factors / 5;
		if ( $count_factors % 5 != 0 ) {

			$pages ++;

		}
		$pages = (int) $pages;

		return $pages;

	}

	public function update_factor( $id, $code ) {

		global $wpdb, $table_prefix;
		$table_name = $table_prefix . 'invoice';
		$this->validation();
		if ( $this->hasError ) {

			return $this;

		}
		$result = $wpdb->update( $table_name, [
			'updated_at'  => $this->updated_at,
			'user_id'     => $this->user_id,
			'title'       => $this->title,
			'description' => $this->description,
			'expired_at'  => $this->expired_at,
			'todo'        => $this->to_do,
			'amount'      => $this->amount
		], [ 'id' => $id, 'code' => $code ] );

		if ( $result ) {

			$this->status_new_invoice = true;

		} else {

			$this->status_new_invoice = false;

		}

		return $this;
	}
	public function get_all_for_posts() {

		global $wpdb, $table_prefix;
		$table_name = $table_prefix . 'invoice';
		$invoices   = $wpdb->get_results( "SELECT * FROM {$table_name} where user_id = 0" );

		return $invoices;

	}



}