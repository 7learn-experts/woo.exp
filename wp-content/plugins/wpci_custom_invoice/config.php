<?php
define( 'WPCI_URL', plugin_dir_url( __FILE__ ) );
define( 'WPCI_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPCI_VIEWS', WPCI_DIR . 'views/' );
define( 'WPCI_ASSETS', WPCI_URL . 'assets/' );


include WPCI_DIR . 'inc/functions.php';
include WPCI_DIR . 'models/WPCI_Invoice.php';
include WPCI_DIR . 'admin/admin.php';
include WPCI_DIR . 'routes/Route.php';
include WPCI_DIR . 'routes/Route_Handler.php';
include WPCI_DIR . 'controllers/factorController.php';



/*foreach ( glob( WPCI_DIR . '/controllers/*.php' ) as $filename ) {
	include $filename;
}

foreach ( glob( WPCI_DIR . '/models/*.php' ) as $filename ) {
	include $filename;
}*/
