<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/css/uikit.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/css/uikit-rtl.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css/styles.css' ?>"/>

<div class="wrap">
    <h2 class="ftBy">لیست فاکتور ها</h2>
    <hr/>



    <table class="uk-table uk-table-striped factors">
		<?php view( 'admin.partials.columns' ) ?>
        <tbody>
		<?php foreach ( $factors as $factor ): ?>

			<?php view( 'admin.partials.columns_data', compact( 'factor' ) ) ?>

		<?php endforeach; ?>


        </tbody>
    </table>
	<?php view( 'admin.partials.pagination', compact( 'pages' ) ) ?>
</div>


<script src="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/js/uikit.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/jquery-3.3.1.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/select2/select2.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/persian_date/persian-date.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/persian_date/persian-datepicker.min.js' ?>"></script>
<script>

    $("a.delete_invoice").click(function (e) {
        e.preventDefault();
        if (confirm('آیا میخواهید این فاکتور را پاک کنید ؟')) {


            window.location.href = $(this).attr('href');


        }

    })

</script>

