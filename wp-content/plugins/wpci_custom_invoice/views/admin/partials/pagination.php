<?php if ( $pages > 1 ): ?>
	<ul class="pagination_factors">
		<?php for ( $i = 1; $i <= $pages; $i ++ ): ?>
			<li>
				<a href="<?php echo add_query_arg( [ 'page_number' => $i ] ) ?>"><?php echo en_number_to_fa_number( $i ) ?></a>
			</li>
		<?php endfor; ?>
	</ul>
<?php endif; ?>