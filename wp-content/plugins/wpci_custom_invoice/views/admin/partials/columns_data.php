<tr>

    <td class="ftTh"><?php echo $factor->code ?></td>
    <td class="ftTh"><?php echo ( isset( $factor->user_nicename ) ) ? $factor->user_nicename : 'چندین کاربر' ?></td>
    <td><?php echo $factor->title ?></td>
    <td><?php echo number_amount( $factor->amount ) ?></td>
    <td><?php echo ( ! empty( $factor->description ) ) ? $factor->description : 'ثبت نشده است' ?></td>


    <td><?php echo status_handler( $factor->status ) ?></td>
    <td><?php echo en_number_to_fa_number( to_jalali( $factor->created_at ) ) ?></td>
    <td><?php echo( isset( $factor->expired_at ) ? to_jalali( fa_number_to_en_number( $factor->expired_at ) ) : 'ثبت نشده است' ) ?></td>
    <td><a uk-tooltip="ویرایش" href="<?php echo add_query_arg( [
			'action' => 'edit',
			'code'   => $factor->code,
			'id'     => $factor->id
		] ) ?>"><span class="dashicons dashicons-edit"></span></a><a uk-tooltip="حذف" class="delete_invoice"
                                                                     href="<?php echo add_query_arg( [
		                                                                 'action' => 'delete',
		                                                                 'code'   => $factor->code,
		                                                                 'id'     => $factor->id
	                                                                 ] ) ?>"><span
                    class="dashicons dashicons-trash"></span></a><a uk-tooltip="لینک"
                                                                    href="/factor/<?php echo $factor->code . '/' . $factor->id ?>"><span
                    class="dashicons dashicons-admin-links"></span></a></td>

</tr>
