<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/css/uikit.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'plugins/select2/select2.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/css/uikit-rtl.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'plugins/persian_date/persian-datepicker.min.css' ?>"/>
<link rel="stylesheet" href="<?php echo WPCI_ASSETS . 'css/styles.css' ?>"/>

<div class="wrap">
	<h2 class="ftBy">ویرایش فاکتور</h2>
	<hr/>
	<div class="container">
		<form action="" method="post">
			<div class="uk-child-width-expand@s uk-text-right" uk-grid>
				<div>
					<div class="field_invoice">
						<label class="ftBy">انتخاب کاربر :</label>

						<select name="user_id" id="js-example-basic-hide-search" class="uk-select uk-form-small">

							<?php foreach ( $users as $user ): ?>

								<option <?php echo ( isset( $status ) && $status != null && $status->user_id == $user->ID ) ? "selected" : (isset($factor) && $factor != null && $factor->user_id == $user->ID) ? "selected" : '' ?>
									value="<?php echo $user->ID ?>"> <?php echo $user->user_login . ' - ' . $user->user_email ?> </option>

							<?php endforeach; ?>

						</select>
					</div>
					<div class="field_invoice">
						<label class="ftBy">عنوان فاکتور :</label>
						<?php
						$cssClass = '';
						$error    = false;
						if ( isset( $status ) && $status != null ) {

							if ( $status->hasError == true && in_array( 'title', $status->errors ) ) {

								$cssClass = 'error_field';
								$error    = true;

							}

						}


						?>
						<?php if ( $error ): ?>

							<span class="errors_data">لطفا عنوان فاکتور را وارد کنید .</span>

						<?php endif; ?>


						<input value="<?php echo ( isset( $status ) && $status != null ) ? $status->title :  (isset($factor) && $factor != null) ? $factor->title : '' ?>"
						       name="title"
						       class="uk-input uk-form-small <?php echo $cssClass ?>" type="text"/>
					</div>
					<div class="field_invoice">
						<label class="ftBy">مبلغ فاکتور :</label>
						<?php
						$cssClass = '';
						$error    = false;
						if ( isset( $status ) && $status != null ) {

							if ( $status->hasError == true && in_array( 'amount', $status->errors ) ) {

								$cssClass = 'error_field';
								$error    = true;

							}

						}


						?>
						<?php if ( $error ): ?>

							<span class="errors_data">لطفا مبلغ را وارد کنید و به صورت عددی .</span>

						<?php endif; ?>

						<input value="<?php echo ( isset( $status ) && $status != null ) ? $status->amount : (isset($factor) && $factor != null) ? $factor->amount : '' ?>"
						       name="amount" class="uk-input uk-form-small <?php echo $cssClass ?>" type="number"/>
					</div>
					<div class="field_invoice">
						<label class="ftBy">تاریخ منقصی شدن :</label>

						<input value="<?php echo ( isset( $status ) && $status != null ) ? $status->expired_at : (isset($factor) && $factor != null) ? to_jalali(fa_number_to_en_number($factor->expired_at)) : '' ?>"
						       class="uk-input uk-form-small dateTime ftBy" name="expired_at" type="text"/>
						<span class="des">در صورت خالی بودن تاریخ انقضا ندارد .</span>
					</div>
					<div class="field_invoice">


						<label class="ftBy">توضیحات :</label>

						<input value="<?php echo ( isset( $status ) && $status != null ) ? $status->description : (isset($factor) && $factor != null) ? $factor->description : '' ?>"
						       name="description" class="uk-input uk-form-small" type="text"/>
					</div>

					<div class="field_invoice">
						<label class="ftBy">انجام چه کاری :</label>

						<select name="todo" class="uk-select uk-form-small ftBy">


							<option value=0">بدون موضوع</option>


						</select>
					</div>

					<button name="edit_factor" type="submit"
					        class="uk-button uk-button-primary uk-button-small ftBy">ویرایش
					</button>
					<button class="uk-button uk-button-primary uk-button-small ftBy" onclick="   <?php header("Refresh:0"); ?>" id="refresh">بازگشت به حالت اولیه</button>
				</div>
				<div>

					<?php if ( isset( $status ) ): ?>

						<?php if ($status->status_new_invoice != null && $status->status_new_invoice == true ): ?>
							<div class="success_new_invoice">

								با موفقیت ویرایش شد

							</div>
						<?php elseif($status->status_new_invoice != null &&$status->status_new_invoice == false): ?>
							<div class="success_new_invoice">

								فاکتور با موفقیت ویرایش نشد .

							</div>
						<?php endif; ?>
					<?php endif; ?>

				</div>
				<div></div>
		</form>
	</div>

</div>

<script src="<?php echo WPCI_ASSETS . 'css_frameworks/UiKit/js/uikit.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/jquery-3.3.1.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/select2/select2.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/persian_date/persian-date.min.js' ?>"></script>
<script src="<?php echo WPCI_ASSETS . 'plugins/persian_date/persian-datepicker.min.js' ?>"></script>

<script>


    $("#js-example-basic-hide-search").select2();

    $(document).ready(function () {
        $(".dateTime").pDatepicker({


            format: 'YYYY/MM/DD',


        });
        $(".dateTime").val("<?php echo ( isset( $status ) && $status != null ) ? $status->expired_at : (isset($factor) && $factor != null) ? to_jalali(fa_number_to_en_number($factor->expired_at)) : '' ?>");


    });



</script>

