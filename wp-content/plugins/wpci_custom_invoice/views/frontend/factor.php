<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>صفحه فاکتور</title>
    <link href="<?php echo WPCI_ASSETS . 'css_frameworks/bootstrap/dist/css/bootstrap.min.css' ?>" rel="stylesheet"/>
    <link href="<?php echo WPCI_ASSETS . 'css_frameworks/bootstrap-v4-rtl/dist/css/bootstrap-rtl.min.css' ?>"
          rel="stylesheet"/>
    <link href="<?php echo WPCI_ASSETS . 'css_frameworks/normalize.css/normalize.css' ?>" rel="stylesheet"/>
    <link href="<?php echo WPCI_ASSETS . 'css/front_end_styles.css' ?>" rel="stylesheet"/>
</head>
<body class="rtl">


<div class="container">

    <div class="row">

        <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12 offset-xl-3 offset-lg-3 offset-md-1  factor-form">
            <form action="" method="post">
                <div class="factor_header">

                    <h4>فاکتور : <?php echo $factor->code ?></h4>
                    <span class="date_time">تاریخ صدور فاکتور : <?php echo en_number_to_fa_number( to_jalali( fa_number_to_en_number( $factor->created_at ) ) ) ?></span>

                </div>
                <div class="factor_body">

                    <span>گیرینده : <?php echo get_bloginfo() ?></span>
                    <input type="hidden" value="<?php echo $data[1] ?>" name="factor_code"/>
                    <input type="hidden" value="<?php echo $data[2] ?>" name="factor_id"/>
                    <input type="hidden" value="<?php echo $data[0] ?>" name="post_id"/>
                    <span>تاریخ انقضا فاکتور : <?php echo en_number_to_fa_number( to_jalali( fa_number_to_en_number( $factor->expired_at ) ) ) ?></span>
                    <span>مبلغ : <?php echo en_number_to_fa_number( number_amount( fa_number_to_en_number( $factor->amount ) ) ) ?>
                        ریال </span>

                    <span>عنوان : <?php echo $factor->title ?></span>

                    <span>توضیحات : <br/>
						<?php echo $factor->description ?></span>

                    <button type="submit" name="submit_data" value="submit_data" class="btn btn-lg btn-success  btn-paid">پرداخت</button>
                </div>
            </form>
        </div>

    </div>

</div>

</body>
</html>