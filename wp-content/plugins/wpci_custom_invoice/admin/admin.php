<?php


function wpci_custom_factor_content_func() {
	global $wpdb, $table_prefix;
	$table_name = $table_prefix . 'invoice';
	$invoice    = new WPCI_Invoice();
	$pages      = $invoice->get_pages();

	$factors = $invoice->get_invoice();
	if ( isset( $_GET['page_number'] ) && $_GET['page_number'] ) {


		$factors = $invoice->get_invoice( $_GET['page_number'], $pages );

	}


	if ( isset( $_GET['action'] ) && $_GET['action'] ) {

		switch ( $_GET['action'] ) {

			case 'delete':

				if ( ( isset( $_GET['code'] ) && $_GET['code'] ) && ( isset( $_GET['id'] ) && $_GET['id'] ) ) {

					$code = $_GET['code'];
					$id   = $_GET['id'];
					$wpdb->delete( $table_name, [ 'id' => $id, 'code' => $code ] );
					$pages   = $invoice->get_pages();
					$factors = $invoice->get_invoice( $_GET['page_number'], $pages );


				}


				break;
			case 'edit':
				$users  = $wpdb->get_results( "SELECT * FROM {$wpdb->users}" );
				$factor = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table_name} WHERE id=%d AND code=%s", $_GET['id'], $_GET['code'] ) );
				if ( isset( $_POST['edit_factor'] ) ) {

					$status               = null;
					$invoice              = new WPCI_Invoice();
					$invoice->user_id     = $_POST['user_id'];
					$invoice->description = $_POST['description'];
					$invoice->title       = $_POST['title'];
					$invoice->to_do       = $_POST['todo'];
					$invoice->expired_at  = to_gregorian( fa_number_to_en_number( $_POST['expired_at'] ) );
					$invoice->updated_at  = date( 'Y-m-d' );
					$invoice->amount      = $_POST['amount'];
					$status               = $invoice->update_factor( $factor->id, $factor->code );
					$factor               = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table_name} WHERE id=%d AND code=%s", $_GET['id'], $_GET['code'] ) );
					wp_redirect( admin_url() . 'admin.php?page=wpci_custom_factor' );
					exit;


				}

				view( 'admin.edit_invoice', compact( 'users', 'factor', 'status' ) );
				exit;


				break;


		}

	}


	view( 'admin.dashboard', compact( 'factors', 'pages' ) );


}


function wpci_custom_factor_content_new_factor_func() {

	global $wpdb;
	$users   = $wpdb->get_results( "SELECT * FROM {$wpdb->users}" );
	$list_op = [
		[

			'id'       => 0,
			'title'    => 'بدون موضوع',
			'function' => ''

		],


	];
	$list_op = apply_filters( 'operation_factor', $list_op );

	$status = null;
	if ( isset( $_POST['create_new_invoice'] ) ) {

		$invoice              = new WPCI_Invoice();
		$invoice->user_id     = $_POST['user_id'];
		$invoice->description = $_POST['description'];
		$invoice->title       = $_POST['title'];
		$invoice->to_do       = $_POST['todo'];
		$invoice->expired_at  = to_gregorian( fa_number_to_en_number( $_POST['expired_at'] ) );
		$invoice->created_at  = date( 'Y-m-d' );
		$invoice->updated_at  = date( 'Y-m-d' );
		$invoice->status      = WPCI_Invoice::NOT_PAID;
		$invoice->amount      = $_POST['amount'];
		$invoice->code        = unique_random_code( 10 );
		$status               = $invoice->create_new_factor();


	}
	view( 'admin.create_factor', compact( 'users', 'status', 'list_op' ) );

}

function wpci_custom_payment_func() {
	echo "خرید ها";
}

function wpci_add_menu_func() {


	add_menu_page(
		'فاکتور سفارشی',
		'فاکتور سفارشی',
		'manage_options',
		'wpci_custom_factor',
		'wpci_custom_factor_content_func',
		'dashicons-clipboard',
		'6'
	);
	add_submenu_page(

		'wpci_custom_factor',
		'صدور فاکتور',
		'فاکتور جدید',
		'manage_options',
		'wpci_new_factor',
		'wpci_custom_factor_content_new_factor_func'

	);
	add_submenu_page(

		'wpci_custom_factor',
		'پرداخت',
		'پرداختی ها',
		'manage_options',
		'wpci_new_payment',
		'wpci_custom_payment_func'

	);


}

add_action( 'admin_menu', 'wpci_add_menu_func' );





