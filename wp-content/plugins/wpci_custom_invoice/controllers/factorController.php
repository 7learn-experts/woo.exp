<?php


function wpci_factor_handler_func( $params = array() ) {

	if ( is_user_logged_in() ) {

		global $wpdb, $table_prefix;
		$table_name  = $table_prefix . 'invoice';
		$table_name1 = $table_prefix . 'payments';
		$factor      = $wpdb->get_row( "SELECT * FROM {$table_name} WHERE id={$params[2]} AND code='{$params[1]}'" );
		$data        = [ $_GET['post_id'], $params[1], $params[2] ];
		if ( isset( $_POST['submit_data'] ) ) {
			$date_time = date( 'Y-m-d H:i:m' );

			$wpdb->insert( $table_name1, array(
				'user_id'    => get_current_user_id(),
				'product_id' => $_GET['post_id'],
				'ref_id'     => 1,
				'res_id'     => 1,
				'factor_id'  => $factor->id,
				'created_at' => "{$date_time}",
				'paid_at'    => "{$date_time}",
				'amount'     => $factor->amount
			) );


			wp_redirect( '/' );

		}

		view( 'frontend.factor', compact( 'factor', 'data' ) );
		exit;

	}


}
