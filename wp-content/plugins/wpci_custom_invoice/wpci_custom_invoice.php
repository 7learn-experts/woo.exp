<?php
/*
Plugin Name: فاکتور سفارشی
Plugin URI: http://mySite.com/
Description: صدور فاکتور سفارشی برای کاربران با قابلیت توسعه سریع و آسان .
Author: AmirHossein Karimi
Version: 1.0.0
Author URI: http://mySite.com/
*/

defined( 'ABSPATH' ) || die( 'access denied !' );


include 'config.php';


function wpci_activation_func() {
	global $wpdb, $table_prefix;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $table_prefix . 'invoice';
	$table_name1     = $table_prefix . 'payments';
	$sql = "CREATE TABLE `$table_name` (
 `id` bigint NOT NULL AUTO_INCREMENT,
 `user_id` bigint(20) NOT NULL,
`code` varchar(255) NOT NULL,
`title` varchar(255) NOT NULL,
 `description` text DEFAULT NULL,
 `created_at` date NOT NULL,
`updated_at` date NOT NULL,
`expired_at` date DEFAULT NULL,
 `paid_at_date` date DEFAULT NULL,
`status` tinyint(4) NOT NULL,
`todo` tinyint(4) NOT NULL,
`amount` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) $charset_collate";

	$sql1 = "CREATE TABLE `$table_name1` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `user_id` bigint(20) NOT NULL,
 `product_id` bigint(20) NOT NULL,
 `ref_id` varchar(100) NOT NULL,
 `res_id` varchar(100) NOT NULL,
 `created_at` datetime NOT NULL,
 `paid_at` datetime NOT NULL,
 `amount` int(11) NOT NULL,
 `factor_id` int(11) NOT NULL,
 PRIMARY KEY (`id`)
)  $charset_collate";


	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	dbDelta( $sql1 );


}

function wpci_deactivation_func() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'invoice';
	$table_name1 = $wpdb->prefix . 'payments';
	$sql        = "DROP TABLE IF EXISTS $table_name";
	$sql1        = "DROP TABLE IF EXISTS $table_name1";
	$wpdb->query( $sql );
	$wpdb->query( $sql1);

}

function wpci_uninstall_func() {


}

register_activation_hook( __FILE__, 'wpci_activation_func' );
register_deactivation_hook( __FILE__, 'wpci_deactivation_func' );
register_uninstall_hook( __FILE__, 'wpci_uninstall_func' );


function wpci_url_handler_func() {


	$current_uri = $_SERVER['REQUEST_URI'];

	$route_list            = [];
	$route                 = new Route();
	$route->route_url      = 'factor\/([A-Za-z0-9]+)\/([0-9]+)\/[?](post_id=)([0-9+])';
	$route->route_function = 'wpci_factor_handler_func';
	$route_list[]          = $route;

	$route_handler = new Route_Handler( $route_list, $current_uri );


}

add_action( 'parse_request', 'wpci_url_handler_func' );