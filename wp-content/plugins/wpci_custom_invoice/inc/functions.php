<?php


function view( $view, $param = array() ) {

	$view = str_replace( '.', '/', $view );
	$path = WPCI_VIEWS . $view . '.php';
	isset( $param ) && $param != null ? extract( $param ) : null;
	if ( file_exists( $path ) && is_readable( $path ) ) {

		include $path;


	}
}


function unique_random_code( $length ) {

	global $wpdb, $table_prefix;
	$table_name = $table_prefix . 'invoice';


	$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen( $characters );
	$randomString     = '';
	for ( $i = 0; $i < $length; $i ++ ) {
		$randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
	}
	$result = $wpdb->get_results( "SELECT * FROM {$table_name} where code={$randomString}" );
	if ( $result && count( $result ) > 1 ) {
		unique_random_code( $length );

	}

	return $randomString;

}


function fa_number_to_en_number( $numbers ) {

	$fa_number = [ '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰' ];
	$en_number = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' ];
	$result    = str_replace( $fa_number, $en_number, $numbers );

	return $result;

}

function en_number_to_fa_number( $numbers ) {

	$fa_number = [ '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰' ];
	$en_number = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' ];
	$result    = str_replace( $en_number, $fa_number, $numbers );

	return $result;

}


function to_gregorian( $date ) {

	if ( $date ) {
		list( $year, $month, $dates ) = explode( '/', $date );

		$dateEn = jalali_to_gregorian( $year, $month, $dates );

		return implode( '-', $dateEn );
	}

	return null;
}

function to_jalali( $date ) {

	if ( $date ) {

		list( $year, $month, $dates ) = explode( '-', $date );

		$dateFa = gregorian_to_jalali( $year, $month, $dates );

		return implode( '/', $dateFa );
	}

	return null;
}


function number_amount( $numbers ) {

	return en_number_to_fa_number( number_format( $numbers ) );

}

function status_handler( $status ) {

	$cssClass = 'notpaid';
	$content  = 'پرداخت نشده';

	if ( $status == 1 ) {

		$cssClass = 'paid';
		$content  = 'پرداخت شده';

	}

	return "<span class='{$cssClass}'>{$content}</span>";

}