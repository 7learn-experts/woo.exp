<?php
/**
 * Created by PhpStorm.
 * User: zoc970301kavorg
 * Date: 6/12/2018
 * Time: 10:59 PM
 */

class Route_Handler {


	public function __construct( $route_lists, $current_uri ) {


		foreach ( $route_lists as $route_list ) {

			if ( preg_match( "/$route_list->route_url/", $current_uri, $matches ) ) {

				$function = $route_list->route_function;
				$function( $matches );

			}

		}


	}

}