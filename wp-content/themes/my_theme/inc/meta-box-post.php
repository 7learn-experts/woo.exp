<?php
function my_theme_price_meta_box() {


	add_meta_box( 'my_theme_price_box', 'قیمیت', 'my_theme_price_func', 'post' );


}

function my_theme_slider_meta_box() {


	add_meta_box( 'my_theme_slider_box', 'اسلایدر', 'my_theme_slider_func', 'post' );


}

function my_theme_price_func( $post ) {
	$post_price = get_post_meta( $post->ID, 'post_price', true );
	?>
    <div class="meta-box-row">
        <input type="text" name="post_price" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $post_price; ?>">
    </div>
	<?php
}

function my_theme_slider_func( $post ) {
	$post_slider = get_post_meta( $post->ID, 'post_slider', true );
	?>
    <div class="meta-box-row">

        <input type="checkbox" name="post_slider" <?php checked( $post_slider ) ?> />
        اسلایدر
    </div>
	<?php
}

function post_saved( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! isset( $_POST['post_price'] ) ) {
		return;
	}

	$post_price  = sanitize_text_field( $_POST['post_price'] );
	$post_slider = isset( $_POST['post_slider'] ) ? true : false;

	update_post_meta( $post_id, 'post_price', $post_price );
	update_post_meta( $post_id, 'post_slider', $post_slider );


}

add_action( 'add_meta_boxes', 'my_theme_price_meta_box' );
add_action( 'add_meta_boxes', 'my_theme_slider_meta_box' );
add_action( 'save_post', 'post_saved' );



