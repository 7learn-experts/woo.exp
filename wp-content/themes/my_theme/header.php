
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Shop Homepage - Start Bootstrap Template</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/bootstrap/dist/css/bootstrap.min.css'?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/bootstrap-v4-rtl/dist/css/bootstrap-rtl.min.css'?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/styles.css'?>">
	<!-- Custom styles for this template -->

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>">



</head>

<body class="rtl">