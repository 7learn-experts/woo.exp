<div class="col-lg-3">
	<?php $categories = get_categories(); ?>
	<div class="list-group my-4">
		<?php foreach ( $categories as $category ): ?>

			<a href="<?php echo  get_category_link($category->term_id)?>" class="list-group-item"><?php echo $category->name ?><span class="badge badge-success float-left catIcon"><?php echo $category->count?></span></a>



		<?php endforeach; ?>
	</div>
</div>