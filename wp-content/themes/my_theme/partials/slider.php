<?php
$args = array(
	'post_type'      => 'post',
	'order'          => 'DESC',
	'posts_per_page' => '3',
	'meta_query'     => array(
		array(
			'key'   => 'post_slider',
			'value' => 1
		)
	)

);

$query = new WP_Query( $args );

?>


<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <!--	<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		</ol>-->
    <div class="carousel-inner" role="listbox">
		<?php $counter = 1; ?>
		<?php foreach ($query->posts as $post): ?>
			<?php if ( $counter == 1 ): ?>
                <div class="carousel-item active">
                    <img class="d-block img-fluid sliderImage" src="<?php echo get_the_post_thumbnail_url($post->ID)?>" alt="First slide">
                </div>
			<?php else: ?>
                <div class="carousel-item ">
                    <img class="d-block img-fluid sliderImage" src="<?php echo get_the_post_thumbnail_url($post->ID)?>" alt="First slide">
                </div>
			<?php endif; ?>
<?php $counter++ ?>
		<?php endforeach; ?>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>