<div class="container">

    <div class="row">

		<?php get_template_part( 'partials/list-category' ) ?>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

			<?php get_template_part( 'partials/slider' ) ?>

            <div class="row">

				<?php if ( have_posts() ): ?>

					<?php while ( have_posts() ): the_post() ?>
					<!--	--><?php
/*
						$have_price = false;
						$price      = get_post_meta( $post->ID, 'post_price', true );
						if ( intval( $price ) > 0 ) {

							$have_price = true;

						}

						*/?>

                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="<?php the_permalink() ?>">


                                    <img class="card-img-top" src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">
                                        <a href="#"><?php the_title() ?></a>
                                    </h5>
									<?php /*if ( $have_price ): */?><!--
                                        <span class="price"> <?php /*echo $price . ' ' */?>تومان </span>
									--><?php /*endif; */?>

                                    <p class="card-text"><?php the_excerpt() ?></p>
                                </div>
                               <!-- <div class="card-footer">
                                    <small class="text-muted"><?php /*if ( $have_price ): */?>

                                            پولی

										<?php /*else: */?>

                                            رایگان
										<?php /*endif; */?>
                                    </small>
                                </div>-->
                            </div>
                        </div>


					<?php endwhile; ?>

				<?php endif; ?>


            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>