<?php


include 'inc/meta-box-post.php';

function filter_function_name( $excerpt ) {
	global $post;
	$post_price = get_post_meta( $post->ID, 'post_price', true );
	if ( intval( $post_price ) > 0 ) {

		return $excerpt . "<br/><span class='badge badge-danger pay'>پولی -- تومان $post_price</span>";
	} else {

		return $excerpt . '<br/><span class="badge badge-success pay">رایگان</span>';

	}


}
function my_custom_setup_theme_func() {

	add_theme_support( 'post-thumbnails' );


}


add_filter( 'get_the_excerpt', 'filter_function_name' );
add_action( 'after_setup_theme', 'my_custom_setup_theme_func' );

