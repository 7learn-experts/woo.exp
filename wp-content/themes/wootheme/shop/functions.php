<?php
function wootheme_single_product_tabs($tabs)
{
//    $tabs['description']['title'] = 'اطلاعات';
    $tabs['technical'] = [
        'title'    => 'مشخصات فنی',
        'priority' => 30,
        'callback' => 'wootheme_technical_tab_content'
    ];

    return $tabs;
}

function wootheme_technical_tab_content()
{
    global $product;
//    var_export($product);
    ?>
    <h1>مشخصات فنی</h1>
    <?php
}

function wootheme_remove_checkout_fields($fields)
{
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
//    unset($fields['billing']['billing_address_1']);
//    unset($fields['billing']['billing_address_2']);
//    unset($fields['billing']['billing_postcode']);
    unset($fields['shipping']['shipping_address_1']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);

    return $fields;
}

add_filter('woocommerce_product_tabs', 'wootheme_single_product_tabs', 98);
add_filter('woocommerce_checkout_fields', 'wootheme_remove_checkout_fields');

function dumpCode($data)
{
    echo '<pre>';
    var_export($data);
    echo '</pre>';
    exit;
}

function wootheme_add_new_item($item)
{
    $item['re-order'] = 'باز سفارش';

    return $item;
}

add_filter('woocommerce_account_menu_items', 'wootheme_add_new_item', 10, 1);
function wootheme_add_re_order_endpoint()
{
    add_rewrite_endpoint('re-order', EP_PAGES);
}

add_action('init', 'wootheme_add_re_order_endpoint');

function wootheme_my_account_re_order_content()
{
    echo '<h1> Re Order</h1>';
}

add_action('woocommerce_account_re-order_endpoint', 'wootheme_my_account_re_order_content');