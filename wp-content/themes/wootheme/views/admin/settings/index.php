<div class="wrap">
	<h1>تنظیمات قالب</h1>
	<form action="" method="post">
		<table class="table-form">
			<tr valign="top">
				<th scope="row">نمایش نوار بالای وب سایت</th>
				<td>
					<input type="checkbox" name="woxp_show_top_bar" <?php checked((int)get_option('woxp_show_top_bar',0),1) ?>>
				</td>
			</tr>
		</table>
		<?php submit_button('ذخیره تنظیمات',null,'saveSettings'); ?>
	</form>
</div>