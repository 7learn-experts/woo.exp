<?php get_header(); ?>
<?php get_template_part('partials/top-header'); ?>
<?php get_template_part('partials/breadcrumb'); ?>
<?php //$post_type = get_post_type(); ?>
    <!-- content page -->
    <section class="bgwhite p-t-60 p-b-25">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-md-12 col-lg-12 p-b-80">
                    <div class="p-r-50 p-r-0-lg">
                        <div class="p-b-40">
                            <?php if (have_posts()): ?>
                                <?php while (have_posts()):the_post(); ?>
                                    <div class="blog-detail-txt p-t-33">
                                        <h4 class="p-b-11 m-text24">
                                            <?php the_title(); ?>
                                        </h4>
                                        <div class="main-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                    <div class="flex-m flex-w p-t-20">
								<span class="s-text20 p-r-20">
									Tags
								</span>

                                        <div class="wrap-tags flex-w">
                                            <?php the_tags(','); ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php get_footer(); ?>