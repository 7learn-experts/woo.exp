jQuery(document).ready(function ($) {

    $('.likePost').on('click', function (event) {
        event.preventDefault();
        var _this = $(this);
        var post_id = _this.data('pid');

        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'like_a_post',
                security:WOXP.ajax_like_token,
                post_id: post_id
            },
            success: function (response) {
                if (response.success) {
                    _this.next().text(response.post_likes_count);
                }
            },
            error: function () {
            }
        }); //[]
    });


});