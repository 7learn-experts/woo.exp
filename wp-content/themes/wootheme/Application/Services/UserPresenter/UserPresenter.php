<?php


namespace Application\Services\UserPresenter;


class UserPresenter
{
    private $user;

    public function __construct($userID)
    {
       $this->user = new \WP_User($userID);
    }

    public function getFullName()
    {
        return  sprintf("%s %s",$this->user->first_name,$this->user->last_name);
    }

    public function getWallet()
    {
        return number_format($this->user->get('wallet'));
    }

}