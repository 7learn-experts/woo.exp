<?php


namespace Application\Services\Cache\Drivers;


use Application\Services\Cache\Contract\CacheContract;

class DB implements CacheContract
{

    public function set($key, $value, $expire)
    {
        return set_transient($key, $value, $expire);
    }

    public function get($key)
    {
        get_transient($key);
    }

    public function delete($key)
    {
        delete_transient($key);
    }
}