<?php


namespace Application\Services\Cache\Drivers;


use Application\Services\Cache\Contract\CacheContract;

class Redis implements CacheContract
{
    private $redisClient;

    public function __construct()
    {
        $this->redisClient = null; // work with Predis
    }

    public function set($key, $value, $expire)
    {
        return $this->redisClient->set($key, $value, $expire);
    }

    public function get($key)
    {
        return $this->redisClient->get($key);
    }

    public function delete($key)
    {
        return $this->redisClient->delete($key);
    }
}