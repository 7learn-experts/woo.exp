<?php


namespace Application\Services\Cache\Drivers;


use Application\Services\Cache\Contract\CacheContract;

class File implements CacheContract
{

    public function set($key, $value, $expire)
    {
        $file_name   = md5($key);
        $expire_date = (new \DateTime())->add(\DateInterval::createFromDateString("PT{$expire}S"));
        $file_name   = $file_name . $expire_date->format("Y-m-d H:i:s") . '.cache';

        return file_put_contents($file_name, $value);
    }

    public function get($key)
    {
        $file_name = md5($key);
        $target    = "";
        foreach (glob("{$file_name}*.cache") as $file) {
            if ( ! empty($file)) {
                $target = $file;
                break;
            }
        }

        return file_get_contents($target);
    }

    public function delete($key)
    {
        $file_name = md5($key);
        $target    = "";
        foreach (glob("{$file_name}*.cache") as $file) {
            if ( ! empty($file)) {
                $target = $file;
                break;
            }
        }
        @unlink($target);
    }
}