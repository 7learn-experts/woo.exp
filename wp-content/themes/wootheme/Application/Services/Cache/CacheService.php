<?php


namespace Application\Services\Cache;


use Application\Services\Cache\Contract\CacheContract;
use Application\Services\Cache\Drivers\DB;
use Application\Services\Cache\Drivers\File;
use Application\Services\Cache\Drivers\Redis;

class CacheService implements CacheContract
{
    private $driver;

    public function __construct($driver = DB::class)
    {
        $this->driver = new $driver;
    }

    public function set($key, $value, $expire)
    {
        return $this->driver->set($key, $value, $expire);
    }

    public function get($key)
    {
        return $this->driver->get($key);
    }

    public function delete($key)
    {
        return $this->driver->delete($key);
    }

    public static function drive($name)
    {
        $driver = "";
        switch ($name) {
            case 'redis':
                $driver = Redis::class;
                break;
            case 'file':
                $driver = File::class;
                break;
            case 'database':
                $driver = DB::class;
                break;
        }

        return new static($driver);
    }
}
//CacheService::drive('redis')->set();