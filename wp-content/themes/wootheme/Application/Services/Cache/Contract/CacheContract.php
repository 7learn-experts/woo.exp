<?php


namespace Application\Services\Cache\Contract;


interface CacheContract
{
    public function set($key, $value,$expire);

    public function get($key);

    public function delete($key);
}