<?php


namespace Application\Services\PostPresenter;


class PostPresenter
{
    private $post;

    public function __construct($postID)
    {
        $this->post = new \WP_Post($postID);
    }

    public function postExcerpt()
    {
        return wp_trim_words($this->post->post_content,60,'<a href="'.get_post_permalink($this->post->ID).'">ادامه مطلب</a>');
    }


}