<?php


namespace Application\Services\Withdrawal;


use Application\Services\UserPresenter\UserPresenter;

class WithdrawalRequest
{
    private $userID;
    private $amount;
    private $registerDate;
    private $targetAccount;

    public function __construct($request)
    {
        $this->userID       = wp_get_current_user()->ID;
        $this->amount       = 5000000;
        $userPresenter = (new UserPresenter($this->userID));
        $this->targetAccount = '6037998156974321';
        $this->registerDate = '2018-09-16 11:20:56';
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    public function register()
    {

    }

    /**
     * @return string
     */
    public function getTargetAccount(): string
    {
        return $this->targetAccount;
    }

}