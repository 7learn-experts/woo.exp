<?php


namespace Application\Services\Withdrawal;


class WithdrawalValidate
{

    private $withdrawalRequest;

    public function __construct(WithdrawalRequest $request)
    {
        $this->withdrawalRequest = $request;
    }

    public function isValid()
    {
        return $this->withdrawalRequest->getAmount() > 1000000;
    }

}