<?php


namespace Application\Services\Withdrawal;


class WithdrawalFacade
{
    private $requestID;

    public function __construct($requestID)
    {
        $this->requestID = $requestID;
    }

    public function perform()
    {
        $request            = $this->getRequest();
        $withdrawalRequest  = new WithdrawalRequest($request);
        $withdrawalValidate = new WithdrawalValidate($withdrawalRequest);
        $withdrawalTransfer = new WithdrawalTransfer($withdrawalRequest);
        if ($withdrawalValidate->isValid()) {
            $withdrawalTransfer->transfer();
        }
    }

    private function getRequest()
    {
        global $wpdb;
        $request = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}withdrawal_requests WHERE request_id=%d",
            $this->requestID));

        return $request;

    }

    public static function withRequestID($requestID)
    {
        return new static($requestID);
    }
}