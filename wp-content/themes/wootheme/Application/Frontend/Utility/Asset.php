<?php


namespace Application\Frontend\Utility;


class Asset
{

    private static $path = ABSPATH;

    public static function css($fileName)
    {
        return self::$path.'/assets/css/'.$fileName.'.css';
    }
}
Asset::css('bootstrap.min');