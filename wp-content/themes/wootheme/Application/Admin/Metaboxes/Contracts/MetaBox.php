<?php


namespace Application\Admin\Metaboxes\Contracts;


abstract class MetaBox
{
    protected $id;
    protected $title;
    protected $screens;
    protected $context = 'normal';
    protected $priority = 'default';

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        add_action('add_meta_boxes',[$this,'registerMetaBox']);
        add_action('save_post',[$this,'saveHandler']);
    }

    public function registerMetaBox()
    {
        add_meta_box(
            $this->id,
            apply_filters('woxp_meta_box_title',$this->title),
            [$this, 'content'],
            $this->screens,
            $this->context
        );
    }
    abstract protected function content($post);
    abstract protected function saveHandler($postID);
}