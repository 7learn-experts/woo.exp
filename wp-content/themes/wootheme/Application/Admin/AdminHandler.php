<?php


namespace Application\Admin;


use Application\Admin\Metaboxes\PriceMetaBox;

class AdminHandler
{
    public function __construct()
    {
        new PriceMetaBox();
    }
}