<?php get_header(); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php //get_template_part( 'partials/blog/blog-header' ); ?>
<!-- content page -->
<section class="bgwhite p-t-60">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9 p-b-75">
                <div class="p-r-50 p-r-0-lg">
					<?php if ( have_posts() ): ?>
						<?php while ( have_posts() ):the_post(); ?>
                            <div class="item-blog p-b-80 wow slideInUp">
                                <a href="<?php the_permalink(); ?>"
                                   class="item-blog-img pos-relative dis-block hov-img-zoom">
									<?php if ( has_post_thumbnail() ): ?>
										<?php the_post_thumbnail( 'full' ); ?>
									<?php else: ?>
                                        <img src="<?php echo woxp_images_url(); ?>blog-03.jpg" alt="IMG-BLOG">
									<?php endif; ?>
                                    <span class="item-blog-date dis-block flex-c-m pos1 size17 bg4 s-text1">
									<?php the_date(); ?>
								</span>
                                </a>
                                <div class="item-blog-txt p-t-33">
                                    <h4 class="p-b-11">
                                        <a href="<?php the_permalink(); ?>" class="m-text24">
											<?php the_title(); ?>
                                        </a>
                                    </h4>
                                    <div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

                                        <span>
										<?php the_category( ',' ); ?>
                                            <span class="m-l-3 m-r-6">|</span>
									</span>

                                        <span>
										8 Comments
                                             <span class="m-l-3 m-r-6">|</span>
									</span>
                                        <span>
                                            <a class="likePost" data-pid="<?php the_ID();?>" href="#"><i class="fa fa-heart"></i></a>
                                            <span><?php echo get_post_meta(get_the_ID(),'_posts_like_count',true); ?></span>
                                        </span>
                                    </div>
                                    <p class="p-b-12">
										<?php the_excerpt(); ?>
                                    </p>
                                    <a href="blog-detail.html" class="s-text20">
                                        خواندن ادامه مطلب
                                        <i class="fa fa-long-arrow-right m-l-8" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
						<?php endwhile; ?>
					<?php endif; ?>

                </div>
                <!-- Pagination -->
                <div class="pagination flex-m flex-w p-r-50">
                    <a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
                    <a href="#" class="item-pagination flex-c-m trans-0-4">2</a>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 p-b-75">
                <div class="rightbar">
					<?php if ( is_active_sidebar( 'main-sidebar' ) ): ?>
						<?php dynamic_sidebar( 'main-sidebar' ); ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
