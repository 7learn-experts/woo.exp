<!-- Slide1 -->
<?php $sliders = woxp_get_sliders(); ?>
<section class="slide1">
    <div class="wrap-slick1">
        <div class="slick1">
			<?php if ( $sliders && count( $sliders ) > 0 ): ?>
				<?php foreach ( $sliders as $slider ): ?>
                    <div class="item-slick1 item1-slick1"
                         style="background-image: url(<?php echo $slider['background']; ?>);">
                        <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
                    <span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15"
                          data-appear="fadeInDown">
							<?php echo $slider['title']; ?>
						</span>

                            <h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37"
                                data-appear="fadeInUp">
								<?php echo $slider['header']; ?>
                            </h2>

                            <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
                                <!-- Button -->
                                <a href="<?php echo $slider['link']; ?>"
                                   class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
									<?php echo $slider['button_text']; ?>
                                </a>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif; ?>

        </div>
    </div>
</section>