<?php get_header(); ?>
<?php get_template_part('partials/top-header'); ?>
<?php get_template_part('partials/big-slider'); ?>
<?php get_template_part('partials/top-banner'); ?>
<?php get_template_part('partials/new-products'); ?>
<?php get_template_part('partials/middle-banners'); ?>
<?php get_template_part('partials/blog'); ?>
<?php get_template_part('partials/instagram'); ?>
<?php get_template_part('partials/shipping'); ?>
<?php get_footer(); ?>

