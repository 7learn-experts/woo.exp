var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var cssMinify = require('gulp-csso');

gulp.task('css', function () {
    return gulp.src('assets/css/*.css')
        .pipe(concatCss('all.css'))
        .pipe(gulp.dest('assets/build/css'));
});
gulp.task('minify',['css'], function () {
    return gulp.src('assets/build/css/all.css')
        .pipe(cssMinify())
        .pipe(gulp.dest('assets/build/css'));
});
gulp.task('default', ['minify']);