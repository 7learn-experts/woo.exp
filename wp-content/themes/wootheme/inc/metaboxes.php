<?php
add_action( 'add_meta_boxes', 'woxp_register_price_meta_box' );
add_action( 'save_post', 'woxp_post_price_handler' );
function woxp_register_price_meta_box() {
	$screens = [
		'post' => 'قیمت مطلب',
		'product' => 'قیمت محصول'
	];
	foreach ( $screens as $screen => $meta_box_title ) {
		add_meta_box(
			'woxp-price-meta-box',
			$meta_box_title,
			'woxp_price_meta_box_content',
			$screen,
			'side'
		);
	}

}

function woxp_price_meta_box_content( $post ) {
	$post_price = get_post_meta( $post->ID, 'woxp_post_price', true );
	woxp_load_view( 'admin.metaboxes.price.index', compact( 'post_price' ) );
}

function woxp_post_price_handler( $post_id ) {
	$post_price = isset( $_POST['woxp_post_price'] ) ? $_POST['woxp_post_price'] : null;
	if ( intval( $post_price ) > 0 ) {
		update_post_meta( $post_id, 'woxp_post_price', $post_price );
	}
}

//add post price custom column
add_filter( 'manage_posts_columns', 'woxp_add_post_price_columns' );
add_action( 'manage_posts_custom_column', 'woxp_display_post_price', 10, 2 );
function woxp_add_post_price_columns( $columns ) {
	$columns['post_price'] = 'قیمت مطلب';

	return $columns;
}

function woxp_display_post_price( $column, $post_id ) {
	if ( $column == 'post_price' ) {
		$post_price = get_post_meta( $post_id, 'woxp_post_price', true );
		if ( empty( $post_price ) ) {
			$post_price = 0;
		}
		echo woxp_format_amount( $post_price ) . ' تومان ';
	}
}

//add post thumbnail custom column
add_filter( 'manage_posts_columns', 'woxp_add_post_thumbnail_columns' );
add_action( 'manage_posts_custom_column', 'woxp_display_post_thumbnail', 10, 2 );
function woxp_add_post_thumbnail_columns( $columns ) {

	return array_merge( [ 'post_thumbnail' => 'تصویر شاخص' ], $columns );

}

function woxp_display_post_thumbnail( $column, $post_id ) {
	if ( $column == 'post_thumbnail' ) {
		echo '<img width="50" height="50" src="' . woxp_get_post_thumbnail_url( $post_id ) . '"/>';
	}
}