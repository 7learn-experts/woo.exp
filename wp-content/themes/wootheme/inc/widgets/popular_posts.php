<?php

class Woxp_Popular_Posts extends WP_Widget {
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'woxp_popular_posts',
			'description' => 'نمایش پر بازدید ترین مطالب',
		);
		parent::__construct( 'woxp_popular_posts', 'پر بازدید ترین مطالب', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		$title = isset( $instance['title'] ) ? $instance['title'] : 'پر بازدید';
		echo $before_widget;
		echo $before_title;
		echo $title;
		echo $after_title;
		$popular_posts = new WP_Query( [
			'meta_key' => 'woxp_post_views',
			'order'    => 'DESC',
			'orderby'=> 'meta_value_num',
//            'posts_per_page' => 10
		] );
		?>
        <div class="woxp_popular_posts">
            <?php if($popular_posts->have_posts()): ?>
                <ul>
                <?php while ($popular_posts->have_posts()):$popular_posts->the_post(); ?>
                        <li>
                            <a href="<?php echo get_the_permalink(); ?>">
                                <?php echo  get_the_title(); ?>
                            </a>
                        </li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
		<?php
		echo $after_widget;

	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : 'پر بازدیدترین مطالب';
		?>
        <label
                for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">عنوان:</label>
        <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>">
		<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = [];
		$instance['title'] = !empty($new_instance['title']) ? $new_instance['title']:'';
		return $instance;
	}

}

add_action( 'widgets_init', function () {
	register_widget( 'Woxp_Popular_Posts' );
} );