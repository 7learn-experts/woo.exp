<?php
function woxp_get_post_views($post_id=0)
{
	if(intval($post_id)==0)
	{
		return 0;
	}

	return (int)get_post_meta($post_id,'woxp_post_views',true);

}
function woxp_update_post_views($post_id= 0)
{

	$current_post_views = woxp_get_post_views($post_id);
	update_post_meta($post_id,'woxp_post_views',++$current_post_views);
}