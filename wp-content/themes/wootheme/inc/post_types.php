<?php
add_action( 'init', 'woxp_register_product_post_type' );
function woxp_register_product_post_type() {
	$labels = array(
		'name'                  => _x( 'محصولات', 'Post type general name', 'textdomain' ),
		'singular_name'         => _x( 'محصول', 'Post type singular name', 'textdomain' ),
		'menu_name'             => _x( 'محصولات', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar'        => _x( 'محصول', 'Add New on Toolbar', 'textdomain' ),
		'add_new'               => __( 'محصول جدید', 'textdomain' ),
		'add_new_item'          => __( 'محصول جدید', 'textdomain' ),
		'new_item'              => __( 'محصول جدید', 'textdomain' ),
		'edit_item'             => __( 'ویرایش محصول', 'textdomain' ),
		'view_item'             => __( 'نمایش محصول', 'textdomain' ),
		'all_items'             => __( 'همه محصولات', 'textdomain' ),
		'search_items'          => __( 'جستجوی محصولات', 'textdomain' ),
		'parent_item_colon'     => __( 'محصولات مادر:', 'textdomain' ),
		'not_found'             => __( 'محصولی یافت نشد.', 'textdomain' ),
		'not_found_in_trash'    => __( 'محصولی در زباله دان یافت نشد.', 'textdomain' ),
		'featured_image'        => _x( 'تصویر محصول', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'Book archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'Books list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'Books list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);
	$args = [
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'product' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
	];
	register_post_type( 'product', $args );
	$is_theme_activated = get_option('woxp_theme_is_active',false);
	if(!$is_theme_activated)
	{
	  flush_rewrite_rules();
		update_option('woxp_theme_is_active',true);
	}
}