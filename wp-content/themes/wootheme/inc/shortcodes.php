<?php
add_shortcode( 'member', 'woxp_members_limit_content' );
add_shortcode( 'downloadBox', 'woxp_show_download_box' );
function woxp_members_limit_content( $atts, $content = "" ) {
	if ( is_user_logged_in() ) {
		return $content;
	}

	return '<div class="member-wrapper"><p>این بخش فقط برای کاربران عضو شده در وب سایت قابل مشاهده می باشد.</p></div>';
}

function woxp_show_download_box( $atts, $content = "" ) {
	$attributes = shortcode_atts( [
		'title' => 'دانلود این فایل',
		'link'  => home_url()
	], $atts, 'downloadBox' );
	if($attributes['link'] == home_url())
	{
		return '';
	}
	return '<div class="download-box"><a href="'.$attributes['link'].'">'.$attributes['title'].'</a></div>';

}
