<?php


abstract class Payment
{
    private $paymentID;
    private $order;
    private $amount;
    private $paidAt;
    public function __construct()
    {
    }
    abstract public function pay();

}
