<?php


class DateParser
{

    private $date;

    public function __construct(DateTime $date)
    {
        $this->date = $date;
    }

    public static function fromString(string $date)
    {

        $object = DateTime::createFromFormat("Y-m-d H:i:s", $date);
        return new self($object);

    }

    public static function fromObject(DateTime $date)
    {

        return new self($date);
    }

}