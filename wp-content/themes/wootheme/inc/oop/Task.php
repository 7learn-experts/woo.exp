<?php

class Task
{

    const UNCOMPLETED = 0;
    const COMPLETED = 1;

    private $title;
    private $description;
    private $status;

    public function __construct(string $title,string $description = "")
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = self::UNCOMPLETED;

    }

    public function getTitle()
    {
        return $this->title;
    }

//    public function setTitle(string $title)
//    {
//        $this->title = $title;
//    }
    public function markAsCompleted()
    {
        $this->status = self::COMPLETED;
    }
}

$task1= new Task("Read PHP Book!");
echo $task1->getTitle();
$task1->markAsCompleted();
$task2 = new Task("Read Css Book");

