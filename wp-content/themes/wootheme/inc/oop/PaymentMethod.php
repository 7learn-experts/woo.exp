<?php


interface PaymentMethod
{
    public function payment(int $paymentID):bool ;
}