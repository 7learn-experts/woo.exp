<?php


class PersianConverter
{
    public static $en_numbers = ['0','1','2','3','4','5','6','7','8','9'];
    public static $fa_numbers = ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];

    public static function numbers($input)
    {
        return str_replace(self::$en_numbers,self::$fa_numbers,$input);
    }

    public static function currency($input)
    {
       return self::numbers(number_format($input));
    }

    public static function toman($input)
    {
        return self::currency($input) . 'تومان';
    }

    public static function hezanToman($input)
    {
        return self::currency($input/1000) . 'هزار تومان';
    }
}