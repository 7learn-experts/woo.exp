<?php
add_action( 'admin_menu', 'woxp_register_theme_options' );
function woxp_register_theme_options() {
	$page_hook = add_theme_page(
		'تنظیمات قالب',
		'تنظیمات قالب',
		'manage_options',
		'woxp_theme_options',
		'woxp_theme_options_handler'
	);
	add_action("load-{$page_hook}",function (){
		add_action('admin_enqueue_scripts',function (){
			wp_register_style('woxp-settings',get_theme_file_uri('assets/css/settings.css'));
			wp_enqueue_style('woxp-settings');
		});
	});
}

function woxp_theme_options_handler() {
	if(isset($_POST['saveSettings']))
	{
		update_option('woxp_show_top_bar',isset($_POST['woxp_show_top_bar']) ? 1 : 0);
	}
	woxp_load_view('admin.settings.index');
}