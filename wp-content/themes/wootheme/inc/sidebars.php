<?php
add_action( 'widgets_init', 'woxp_register_sidebars' );
function woxp_register_sidebars() {
	register_sidebar( [
		'name'          => 'سایدبار اصلی',
		'id'            => 'main-sidebar',
		'description'   => 'ساید بار اصلی قالب ',
		'before_widget' => '<div class="main-sidebar-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="m-text23 p-t-65 p-b-34">',
		'after_title'   => '</h4>'
	] );
}