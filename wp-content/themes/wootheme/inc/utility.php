<?php
function woxp_load_asset( $file_name = null ) {
	if ( is_null( $file_name ) ) {
		woxp_assets_url();
	}

	return woxp_assets_url() . $file_name;
}

function woxp_assets_url() {
	return get_template_directory_uri() . '/assets/';
}

function woxp_images_url() {
	return woxp_assets_url() . 'images/';
}

function woxp_format_amount( $amount ) {

	$result = number_format( $amount );

	return woxp_persian_number( $result );
}

function woxp_persian_number( $input ) {
	$persian_numbers = [ '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' ];
	$en_numbers      = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];

	return str_replace( $en_numbers, $persian_numbers, $input );

}

function woxp_get_post_thumbnail_url($post_id,$size='thumbnail')
{
	if(has_post_thumbnail($post_id))
	{
		return get_the_post_thumbnail_url($post_id,$size);
	}
	return get_template_directory_uri().'/assets/images/default.png';
}

function woxp_load_view( $view, $params = array() ) {
	$view         = str_replace( '.', DIRECTORY_SEPARATOR, $view );
	$viewFilePath = get_template_directory().'/views/' . $view . '.php';
	if ( file_exists( $viewFilePath ) && is_readable( $viewFilePath ) ) {
		! empty( $params ) ? extract( $params ) : null;
		include $viewFilePath;
	}
}
