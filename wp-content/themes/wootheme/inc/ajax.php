<?php
add_action( 'wp_ajax_like_a_post', 'woxp_like_a_post' );
add_action( 'wp_ajax_nopriv_like_a_post', 'woxp_like_a_post' );

function woxp_like_a_post() {
	check_ajax_referer('ajax_like_action','security');
	$post_id = intval( $_POST['post_id'] );
	if ( $post_id ) {
		$current_post_likes_count = (int) get_post_meta( $post_id, '_posts_like_count', true );
		update_post_meta( $post_id, '_posts_like_count', ++ $current_post_likes_count );
		wp_send_json( [
			'success'          => true,
			'post_likes_count' => $current_post_likes_count
		] );

	}
	wp_send_json( [
		'success' => false
	] );
}