<?php
function woxp_add_sliders_page() {
	add_menu_page(
		'اسلایدر',
		'اسلایدر',
		'manage_options',
		'woxp_sliders',
		'woxp_sliders_handler'
	);
}

function woxp_sliders_handler() {

}

add_action( 'admin_menu', 'woxp_add_sliders_page' );

function woxp_get_sliders( $count = 3 ) {
	global $wpdb, $table_prefix;
	$sliders = $wpdb->get_results( "
		SELECT * 
		FROM {$table_prefix}sliders
		LIMIT {$count}
	" );
	$result  = [];
	if ( $sliders && count( $sliders ) > 0 ) {
		foreach ( $sliders as $slider ) {
			$result[] = [
				'title'       => $slider->slider_title,
				'header'      => $slider->slider_header,
				'button_text' => $slider->slider_button_text,
				'link'        => $slider->slider_button_url,
				'background'  => $slider->slider_background
			];
		}
	}

	return $result;
}