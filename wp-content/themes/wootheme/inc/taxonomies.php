<?php
add_action('init','woxp_register_product_category_taxonomy');
function woxp_register_product_category_taxonomy()
{
	$labels = array(
		'name'              => _x( 'دسته بندی محصولات', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'دسته بندی محصول', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'جستجوی دسته بندی محصولات', 'textdomain' ),
		'all_items'         => __( 'همه دسته بندی های محصولات', 'textdomain' ),
		'parent_item'       => __( 'دسته بندی محصول والد', 'textdomain' ),
		'parent_item_colon' => __( 'دسته بندی محصول والد:', 'textdomain' ),
		'edit_item'         => __( 'ویرایش دسته بندی محصول', 'textdomain' ),
		'update_item'       => __( 'به روز رسانی دسته بندی محصول', 'textdomain' ),
		'add_new_item'      => __( 'اضافه کردن دسته بندی محصول ', 'textdomain' ),
		'new_item_name'     => __( 'دسته بندی محصول جدید', 'textdomain' ),
		'menu_name'         => __( 'دسته بندی محصول', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product_category' ),
	);

	register_taxonomy( 'product_category', array( 'product' ), $args );
}