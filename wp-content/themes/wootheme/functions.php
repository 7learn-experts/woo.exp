<?php
use Application\Services\Withdrawal\WithdrawalFacade;

spl_autoload_register('autoload');

function autoload($className)
{
    $classFilePath         = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    $classFilePath         .= '.php';
    $classFileCompletePath = get_theme_file_path($classFilePath);
    if (file_exists($classFileCompletePath) && is_readable($classFileCompletePath)) {

        include_once $classFileCompletePath;
    }
}

function woxp_setup_theme()
{
    add_theme_support('woocommerce');
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_image_size('blog-thumbnail', 820, 480);
    register_nav_menu('top-menu', 'Top Menu');
    //remove_action('woocommerce_before_main_content','woocommerce_output_content_wrapper');
    //add_filter('woocommerce_show_page_title','__return_false');
    //remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering',30);
    add_filter('loop_shop_columns',function(){return 3;});
}

function woxp_setup_assets()
{
    if ( ! is_admin()) // is_single is_home is_category is_author
    {
        wp_register_style(
            'animate-css',
            get_template_directory_uri() . '/assets/css/animate.css'
        );
        wp_enqueue_style('animate-css');
        wp_register_script('wow-js',
            get_template_directory_uri() . '/assets/js/wow.min.js',
            ['jquery'], null, true
        );
        wp_register_script('woxp-scripts', get_theme_file_uri('assets/js/theme.js'), ['jquery'], false, true);
        wp_localize_script('woxp-scripts', 'WOXP', [
            'ajax_like_token' => wp_create_nonce('ajax_like_action'),

        ]);
        wp_enqueue_script('woxp-scripts');

        wp_enqueue_script('wow-js');

    }

}

add_action('after_setup_theme', 'woxp_setup_theme');
add_action('wp_enqueue_scripts', 'woxp_setup_assets');
add_filter('show_admin_bar', '__return_false');
function cubiq_setup()
{
    remove_action('wp_head', 'wp_generator');                // #1
    remove_action('wp_head', 'wlwmanifest_link');            // #2
    remove_action('wp_head', 'rsd_link');                    // #3
    remove_action('wp_head', 'wp_shortlink_wp_head');        // #4

    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);    // #5

    add_filter('the_generator', '__return_false');            // #6
    add_filter('show_admin_bar', '__return_false');            // #7

    remove_action('wp_head', 'print_emoji_detection_script', 7);  // #8
    remove_action('wp_print_styles', 'print_emoji_styles');
}

add_action('after_setup_theme', 'cubiq_setup');
include "inc/utility.php";
include "inc/slider.php";
include "inc/shortcodes.php";
include "inc/posts_meta.php";
//include "inc/metaboxes.php";
include "inc/sidebars.php";
include "inc/widgets.php";
//include "inc/post_types.php";
//include "inc/taxonomies.php";
include "inc/settings.php";
include "inc/ajax.php";
include "shop/functions.php";
if (is_admin()) {
    new \Application\Admin\AdminHandler();
}

if ( ! function_exists('someFunction')) {
    function someFunction()
    {
        WithdrawalFacade::withRequestID(123)->perform();
    }
}

//add_filter('locale', function () {
//    $ln = $_GET['ln'];
//
//    return getLocalesByCode($ln);
//});

function getLocalesByCode($code)
{
    return [
        'en' => 'en_US',
        'fa' => 'fa_IR',
        'ar' => 'ar_SU'
    ][$code];
}