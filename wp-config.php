<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'woo_experts');

/** MySQL database username */
define('DB_USER', 'root');


/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l5Y|GMqe]w4sZx.gu!l!1RN{>I`m.3Fv~uFz]uDvsdE#A,To_&$S~Y[]@,5pI;?P');
define('SECURE_AUTH_KEY',  'O0`29<s9$u0~R~y(2j4!lC:BOUG~(Qym:,TXG>3[4%j8b80$I*OnduJn59rgpoFt');
define('LOGGED_IN_KEY',    'kh!wyjbRPvP*VN.:6 |{?LlDAit(Ru|%r.rUl3wnaQ@pH0(u{`glR47sv*Sg2T37');
define('NONCE_KEY',        'iJK /`F&Meg1~d6-2>wk,vD(b-.abe&BYN^Gq@<qY UVE;}-w$k@W%+2EC4bgoUB');
define('AUTH_SALT',        '|nahW>r-TU3^#dy:`(f>;Q#j:0|43?xjAA:?8|l(+S)TMb1,+mT#F.PyEngf8-YO');
define('SECURE_AUTH_SALT', 'VX|pIzgAd1Cs?)KzJe[vA^3gMP8[l|4D_k#Wzi=b&Bh>k6AM%1M|}^UmKc~e6FBA');
define('LOGGED_IN_SALT',   'O16nb0B/_hCr7;AdSNCn^9}k0TqUf*8:UH0ESK-;`SA=HS|RNc$TBbm$UJ`<& ?;');
define('NONCE_SALT',       'v2>nNEna//wx}f&BqjPriA G)Uw!`#B26Y@P}xc&CmSBHt(iV.yV{+w;N#mu-N[S');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'woo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
